<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Vendor;
use App\User;
use App\UserAuth;


class CreateUserBatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'galihs:createuser {mode} {name?} {email?} {groupap?} {module?} {submodule?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create All Vendor User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mode = $this->argument('mode');
        $name = $this->argument('name');
        $email = $this->argument('email');
	$groupap = $this->argument('groupap');
	$module = $this->argument('module');
	$submodule = $this->argument('submodule');

        if($mode == 'individual') {
            // random password
            $alphabet = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
            $generatedpassword = substr(str_shuffle($alphabet), 0, 6);

            // bikin user
            try {
                $user = User::create([
                    'name' => $name,
                    'email' => $email,
                    'email_verified_at' => Carbon::now(),
                    'password' => Hash::make($generatedpassword),
                    'groupap' => $groupap
                ]);

                // user auth
                $userauth = UserAuth::create([
                    'userid' => $user->id,
                    'module' => $module,
                    'submodule' => $submodule
                ]);

                $this->info('User created: ' . $email . ' : ' . $generatedpassword);
            }
            catch(\Exception $e) {
                $this->error($e);
            }
        }
        else {
            // select vendor yang belum punya database
            $vendors = Vendor::with(['sapvendor'])
                    ->whereNull('clearpasswd')
                    ->get();
            foreach ($vendors as $vendor) {
                // email
                $sapvendor = $vendor->sapvendor;
                if($sapvendor == null) {
                    continue;
                }
                
                $email = explode(',', $sapvendor->email);
                if($email[0] == '') {
                    continue;
                }

                // random password
                $alphabet = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
                $generatedpassword = substr(str_shuffle($alphabet), 0, 6);

                // bikin user
                try {
                    $user = User::create([
                        'name' => $sapvendor->name,
                        'email' => $email[0],
                        'email_verified_at' => Carbon::now(),
                        'password' => Hash::make($generatedpassword),
                        'groupap' => $vendor->id
                    ]);

                    // update datanya
                    $vendor->clearpasswd = $generatedpassword;
                    $vendor->useremail = $email[0];
                    $vendor->save();

                    // user auth
                    $userauth = UserAuth::create([
                        'userid' => $user->id,
                        'module' => 'VENDORINPUT',
                        'submodule' => 'VENDORINPUT'
                    ]);

                    $this->info('User created.');
                }
                catch(\Exception $e) {
                    continue;
                }
            }
        }
    }
}
