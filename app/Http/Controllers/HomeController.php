<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

use App\Documents;
use App\WfHistory;
use App\VerifyUser;
use App\User;
use App\MeetingRoomSchedule;
use App\KpiHeader;

use GuzzleHttp\Client;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function signage()
    {
        $today = Carbon::now()->format('d F Y');
        $schedules = MeetingRoomSchedule::where('meetingdate', '=', Carbon::now()->format('Y-m-d'))
                        ->orderby('roomid')
                        ->orderby('starttime')
                        ->get();
        
        $itemsperpage = 5;
        $pages = ceil(count($schedules) / $itemsperpage);
        return view('signage', [
            'today' => $today,
            'schedules' => $schedules,
            'pages' => $pages,
            'itemsperpage' => $itemsperpage,
        ]);
    }

    public function sendpush()
    {
        $assignee = 'galih.satriaji';
        // cari user devices
        $user = User::with(['devices'])
                    ->where('email', 'like', $assignee . '%')
                    // ->where('subscribed', '1')
                    ->first();
        $ONESIGNALURL = 'https://onesignal.com/api/v1/notifications';
        foreach ($user->devices as $device) {
            // kirim push notifikasi ke sini
            $playerids = array($device->deviceid);
            $client = new Client();
            try {
                $resp = $client->post($ONESIGNALURL, [
                    'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                    'body' => '{
                        "app_id": "7f2d9523-b996-4484-a9e8-ec75de4d2470",
                        "headings": {"en": "New Workflow Assignment!"},
                        "contents": {"en": "Ada assignment baru yang perlu segera di-follow-up"},
                        "include_player_ids": ' . json_encode($playerids) . '
                    }'
                ]);
                Log::info($resp->getBody());
            } catch (\Throwable $th) {
                Log::error($th);
                continue;
            }
        }
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser)) {
            $user = $verifyUser->user;
            if(!$user->email_verified_at) {
                $verifyUser->user->email_verified_at = \Carbon\Carbon::now();
                $verifyUser->user->save();
                $status = "Your e-mail is verified. You can now login using mobile app.";
            }
            else {
                $status = "Your e-mail is already verified. You can now login using mobile app";
            }
        }
        else {
            $status = "Sorry your email cannot be identified.";
        }
        return $status;
    }

    public function getQrCode($whid)
    {
        $url = env('APP_URL');

        $wfhistory = WfHistory::find($whid);

        $qrtext = "
            $wfhistory->status | 
            $wfhistory->updatedby |
            $wfhistory->memo |
            Verifikasi: $url/verifyqrcode/$wfhistory->id
        ";

        $pngImage = \QrCode::format('png')->merge('logo.png', 0.15, true)
                        ->size(200)->errorCorrection('H')
                        ->generate($qrtext);
 
        return response($pngImage)->header('Content-type','image/png');
    }

    public function verifyQrCode($whid)
    {
        $wfhistory = WfHistory::findOrFail($whid);
        return $wfhistory;
    }

    public function civd()
    {
        $companies = DB::table('gs_temp')->get();
        $retval = [];
        foreach($companies as $c) {
            $civd = DB::table('civd_vendor')
                        ->where('namaentitas', 'like', '%' . trim($c->name) . '%')
                        ->first();
            if($civd != null)                        
                array_push($retval, $civd);
        }

        return view('civd', [
            'vendor' => $retval
        ]);
        // return $retval;
    }

    public function downloadRfqAttachment($docid)
    {
        $document = Documents::find($docid);

        return Storage::download('rfq/' . $document->ownerid . '/' . $document->docname);
    }

    public function downloadAttachment($docid, $handle = 'VENDOR')
    {
        $document = Documents::find($docid);

        if($handle == 'KPI') {
            $kpiheader = KpiHeader::find($document->ownerid);
            return Storage::download('kpi/' . $kpiheader->apgroup . '-' . $kpiheader->period . '/' . $document->docname);
        }

        return Storage::download('vendor/' . $document->ownerid . '/' . $document->docname);
    }
}
