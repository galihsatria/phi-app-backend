<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Vehicle;
use App\Booking;
use App\BookingLine;
use App\Driver;
use App\ValueList;
use App\MeetingRoom;
use App\MeetingRoomSchedule;

use App\Mail\BookingNotificationEmail;
use App\Jobs\StartWorkflowApproval;

class GeneralAffairController extends Controller
{

    public function __construct()
    {
        // $this->middleware(['auth', 'verified']);
        $this->middleware('auth');
    }

    public function deleteSchedule($scheduleid)
    {
        MeetingRoomSchedule::find($scheduleid)->delete();

        // reload
        $schedules = MeetingRoomSchedule::with(['room'])
                        ->where('meetingdate', '>=', Carbon::now()->format('Y-m-d') )
                        ->orderby('roomid')
                        ->orderby('starttime')
                        ->get();
        return response()->json([
            'status' => 'SUCCESS',
            'schedules' => $schedules
        ]);
    }

    public function deleteRoom($roomid)
    {
        MeetingRoom::find($roomid)->delete();
        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function saveSchedule(Request $request)
    {
        $schedule = MeetingRoomSchedule::firstOrNew([
            'id' => $request->scheduleid,
        ]);
        $schedule->name = $request->schedulename;
        $schedule->organizer = $request->organizer;
        $schedule->roomid = $request->roomid;
        $schedule->meetingdate = $request->meetingdate;
        $schedule->starttime = $request->starttime;
        $schedule->finishtime = $request->finishtime;
        $schedule->quantity = $request->quantity;
        $schedule->save();

        $sched = MeetingRoomSchedule::with(['room'])->find($schedule->id);

        return response()->json([
            'status' => 'SUCCESS',
            'schedule' => $sched
        ]);
    }

    public function saveRoom(Request $request)
    {
        $meetingroom = MeetingRoom::firstOrNew([
            'id' => $request->roomid,
        ]);
        $meetingroom->name = $request->name;
        $meetingroom->floor = $request->floor;
        $meetingroom->capacity = $request->capacity;
        $meetingroom->save();

        return response()->json([
            'status' => 'SUCCESS',
            'room' => $meetingroom
        ]);
    }

    public function meetingroom()
    {
        $meetingrooms = MeetingRoom::orderby('floor', 'name')->get();
        $schedules = MeetingRoomSchedule::with(['room'])
                        ->where('meetingdate', '>=', Carbon::now()->format('Y-m-d') )
                        ->orderby('roomid')
                        ->orderby('starttime')
                        ->get();

        return response()->json([
            'status' => 'SUCCESS',
            'meetingrooms' => $meetingrooms,
            'schedules' => $schedules
        ]);
    }

    public function updateAvatar(Request $request)
    {
        return response()->json([
            'status' => 'SUCCESS',
            'avatar' => 'avatar'
        ]);
    }

    public function destinations()
    {
        $destinations = ValueList::where('category', 'DESTINATIONS')
                            ->orderby('name')
                            ->get();
        return response()->json([
            'status' => 'SUCCESS',
            'destinations' => $destinations
        ]);
    }

    public function deleteDriver($driverid)
    {
        Driver::find($driverid)->delete();
        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function deleteMobil($vehicleid)
    {
        Vehicle::find($vehicleid)->delete();
        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function getBookingRequest()
    {
        // query nya
        $activebook = Booking::with(['passengers', 'requestor', 'passengers.userdepartment'])
                        ->where('wfstatus', 'APPR')
                        ->orderby('updated_at', 'desc')
                        ->get();
        return response()->json([
            'status' => 'SUCCESS',
            'carrequest' => $activebook
        ]);
    }

    public function completeBook(Request $request)
    {
        $b = Booking::find($request->bookingid);
        $b->origin = $request->origin;
        $b->destination = $request->destination;
        $b->departuredate = $request->departuredate;
        $b->returndate = $request->returndate;
        $b->quantity = $request->quantity;
        $b->remarks = $request->remarks;

        // completed by transport
        $b->wfstatus = 'ASSIGNED';
        $b->assignedcarid = $request->assignedcar;
        $b->assigneddriverid = $request->assigneddriver;
        $b->transportremarks = $request->transportremarks;
        $b->save();

        // refresh
        $updatedbook = Booking::with(['passengers', 'requestor', 'passengers.userdepartment'])
                        ->where('id', $b->id)
                        ->first();
        
        // kirim notifikasi ke requestor
        Mail::to($b->requestor->email)->queue(new BookingNotificationEmail($b));

        // start workflow untuk si driver
        // start workflow to CAMUNDA asynchronously
        // preparing payload
        $payload = new \stdClass();
        $payload->wfname = 'DRIVER';
        $payload->processid = 'DriverWorkflow';
        $payload->ownertable = 'BOOKING';
        $payload->ownerid = $b->id;
        $payload->status = 'ASSIGNED';
        $payload->updatedby = Auth::user()->ldapname;

        // initiator
        $initiator = new \stdClass();
        $initiator->type = 'string';
        $initiator->value = Auth::user()->ldapname;

        $bpmVariables = new \stdClass();
        $bpmVariables->initiator = $initiator;

        // driver -- find
        $driver = Driver::find($request->assigneddriver);
        $email = $driver->user->email;
        $explodedEmail = explode('@', $email);
        $driverObj = new \stdClass();
        $driverObj->type = 'string';
        $driverObj->value = $explodedEmail[0];
        $bpmVariables->driver = $driverObj;

        // external api
        $extapi = new \stdClass();
        $extapi->type = 'string';
        $extapi->value =  'http://localhost/phiappb/api';
        $bpmVariables->EXTAPI = $extapi;

        $payload->variables = $bpmVariables;
        
        StartWorkflowApproval::dispatch($payload);

        return response()->json([
            'status' => 'SUCCESS',
            'booking' => $updatedbook
        ]);
    }

    public function getMobil()
    {
        $vehiclelist = Vehicle::get();
        $driverlist = Driver::get();
        return response()->json([
            'status' => 'SUCCESS',
            'vehiclelist' => $vehiclelist,
            'driverlist' => $driverlist,
        ]);
    }

    public function saveDriver(Request $request)
    {
        $driver = Driver::firstOrNew([
            'id' => $request->driverid
        ]);
        $driver->name = $request->name;
        $driver->email = $request->email;
        $driver->phone = $request->phone;
        $driver->save();

        return response()->json([
            'status' => 'SUCCESS',
            'driver' => $driver
        ]);
    }

    public function saveMobil(Request $request)
    {
        $vehicle = Vehicle::firstOrNew([
            'id' => $request->vehicleid,
        ]);
        $vehicle->vehicletype = $request->vehicletype;
        $vehicle->nomorpolisi = $request->nomorpolisi;
        $vehicle->capacity = $request->capacity;
        $vehicle->vendor = $request->vendor;
        $vehicle->status = $request->status;
        $vehicle->save();

        return response()->json([
            'status' => 'SUCCESS',
            'vehicle' => $vehicle
        ]);
    }
}
