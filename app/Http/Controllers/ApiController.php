<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

use GuzzleHttp\Client;

use App\Mail\VerifyMail;

use App\BidControl;
use App\User;
use App\Vendor;
use App\SapVendor;
use App\Rfq;
use App\RfqLine;
use App\WfApproval;
use App\WfHistory;
use App\UserRole;
use App\Report;
use App\Documents;
use App\VendorLetter;
use App\VendorCommodities;
use App\BidEval;
use App\TechnicalEval;
use App\Baep;
use App\ValueList;
use App\RfqMilestone;
use App\Department;
use App\KpiMaster;
use App\KpiHeader;
use App\KpiLine;
use App\VerifyUser;
use App\UserDevice;
use App\VendorVerification;

class ApiController extends Controller
{
    protected $WORKFLOW_ENGINE_URL = 'http://localhost:8080/engine-rest';
    protected $HTTP_HEADERS = ['Content-Type' => 'application/json', 'Accept' => 'application/json'];
    

    // ========================== REACTJS CLIENT RELATED ===========

    public function dashboard()
    {
        $uploadedvendor = DB::select("
            select count(*) total from phivendor where id in (select ownerid from phidocuments where owner = 'VENDOR')
        ");

        $verifiedvendor = DB::select("
            select count(*) total from phivendor where verified = '1'
        ");

        $totalvendor = DB::select("
            select count(*) total from phivendor
        ");

        $rfqinprogress = DB::select("
            select count(*) total from rfq
            where 1 = 1
            and id in (select ownerid from wfapproval where ownertable = 'RFQ' and status not in ('APPR', 'DRAFT', 'CANCEL'))
        ");

        // inbox assignment
        $user = Auth::user();

        $email = $user->email;
        $explodedEmail = explode('@', $email);

        // request ke BPM
        $TASKURL = $this->WORKFLOW_ENGINE_URL . '/task?assignee=' . $explodedEmail[0];
        $client = new Client();
        $instanceids = [];

        try {
            $r = $client->get($TASKURL, [
                'headers' => $this->HTTP_HEADERS
            ]);
    
            $body = json_decode($r->getBody(), true);

            foreach($body as $b) {
                $instanceids[] = $b['processInstanceId'];
            }
        }
        catch(\GuzzleHttp\Exception\RequestException $e) {
            // 
        }

        // ini harus dicek dengan active wfapproval
        $wfapproval = WfApproval::whereIn('instanceid', $instanceids)
                        ->where('status', '!=', 'APPR')
                        ->get();

        return response()->json([
            'status' => 'SUCCESS',
            'verifiedvendor' => $verifiedvendor[0],
            'uploadedvendor' => $uploadedvendor[0],
            'totalvendor' => $totalvendor[0],
            'rfqinprogress' => $rfqinprogress[0],
            'wfapproval' => $wfapproval
        ]);
    }

    // ===
    public function updatePushId(Request $request)
    {
        UserDevice::firstOrCreate([
            'userid' => Auth::id(),
            'deviceid' => $request->deviceid
        ]);

        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function getMyProfile()
    {
        $user = Auth::user();

        return response()->json([
            'status' => 'SUCCESS',
            'user' => $user
        ]);
    }

    public function kpiDeleteFile($docid)
    {
        $document = Documents::find($docid);

        $ownerid = $document->ownerid;
        $kpiheader = KpiHeader::find($ownerid);

        Storage::delete('kpi/' . Auth::user()->groupap . '-' . $kpiheader->period . '/' . $document->docname);
        $document->delete();

        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function kpiattachmentlist($kpiheaderid)
    {
        $kpiattachmentlist = DB::select("
            select vl.name as name, vl.description as description, d.docname, d.id from valuelist vl 
                left join phidocuments d on d.category = vl.name and d.owner = 'KPIHEADER' and d.ownerid = ? 
            where vl.category = 'KPIATTACHMENT' 
            order by vl.id
        ", [$kpiheaderid]);

        return response()->json([
            'status' => 'SUCCESS',
            'attachmentlist' => $kpiattachmentlist
        ]);
    }

    public function kpidetail(Request $request)
    {
        $kpiAuth = DB::select("
            select * from user_auth where userid = ?
            and module = 'KPI' and submodule = 'ALL'
        ", [Auth::user()->id]);
        
        $apgroup = count($kpiAuth) > 0 ? "'PHI', 'PHM', 'PHSS', 'PHKT'" : "'" . Auth::user()->groupap . "'";

        $kpiheaderlist = DB::select("
            select * from kpiheader where apgroup in (" . $apgroup . ") order by created_at desc
        ");

        return response()->json([
            'status' => 'SUCCESS',
            'kpiheaderlist' => $kpiheaderlist
        ]);
    }

    public function kpidashboard()
    {
        $availableperiods = ValueList::where('category', 'KPIPERIODS')
                                ->select('name')
                                ->pluck('name')
                                ->toArray();

        // kalo belum ada data
        $count = KpiHeader::count();
        if($count == 0) {
            return response()->json([
                'status' => 'SUCCESS',
                'periods' => $availableperiods,
                'chartoptions' => []
            ]);
        }

        // build the charts
        $kpilist = KpiMaster::orderby('id')->get();

        $chartoptions = [];

        // build series
        $kpiAuth = DB::select("
            select * from user_auth where userid = ?
            and module = 'KPI' and submodule = 'ALL'
        ", [Auth::user()->id]);
        
        $apgroup = count($kpiAuth) > 0 ? "'PHI', 'PHM', 'PHSS', 'PHKT'" : "'" . Auth::user()->groupap . "'";
        $groupAp = count($kpiAuth) > 0 ? 'ALL' : Auth::user()->groupap;
        
        foreach ($kpilist as $kpi) {
            $maxinvertal = $kpi->targetmax + (0.2 * $kpi->targetmax);

            $chartOption = new \stdClass();

            $chartOption->credits = 'false';

            $charttypeobj = new \stdClass();
            $charttypeobj->type = 'column';
            $chartOption->chart = $charttypeobj;

            $titleobj = new \stdClass();
            $titleobj->text = $kpi->description;
            $chartOption->title = $titleobj;

            $seriesgroup = DB::select("
                select distinct apgroup from kpiheader where apgroup in (" . $apgroup . ")
            ");

            $series = [];
	        $categories = [];
            $chartOption->series = $series;
            foreach ($seriesgroup as $group) {
                $seriesEl = new \stdClass();
                $seriesEl->name = $group->apgroup;

                // get the series
                $seriesdata = DB::select("
                    SELECT * FROM kpiheader h, kpiline l where h.id = l.kpiheaderid and h.apgroup = ? and l.kpiid = ?
                    order by h.created_at
                ", [$group->apgroup, $kpi->id]); // [Auth::user()->groupap, $kpi->id]);

                // Log::info(print_r($seriesdata, true));
		        $xvalue = [];
		        $categories2 = [];
                foreach ($seriesdata as $data) {
		            array_push($categories2, $data->period);
                    array_push($xvalue, $data->value);
                }
                $seriesEl->data = $xvalue;
		        array_push($series, $seriesEl);

                if(count($categories2) > count($categories)) {
                $categories = $categories2;
                }
            }
            $chartOption->series = $series;

            $xAxis = new \stdClass();
            $xAxis->categories = $categories;
            $chartOption->xAxis = $xAxis;

            $yAxis = new \stdClass();
            $yAxis->min = 0;
            if($kpi->targetmax !== null) {
                $yAxis->max = $maxinvertal;
            }
            

            $yAxisTitle = new \stdClass();
            $yAxisTitle->text = $kpi->measure;
            $yAxis->title = $yAxisTitle;
            
            $plotBands = [];

            if($kpi->targetmin !== null) {
                // plot band bawah
                $lowerPlotBand = new \stdClass();
                $lowerPlotBand->color = ($kpi->inverted == 1) ? '#ffeded' : '#f4fff2'; // red - green
                $lowerPlotBand->from = $kpi->targetmin;
                $lowerPlotBand->to = ($kpi->targetmed > 0) ? $kpi->targetmed : $kpi->targetmax;
                array_push($plotBands, $lowerPlotBand);

                // higher plot band
                $upperPlotBand = new \stdClass();
                $upperPlotBand->color = ($kpi->inverted) ? '#f4fff2' : '#ffeded'; // green - red
                $upperPlotBand->from = ($kpi->targetmed > 0) ? $kpi->targetmax : $maxinvertal;
                $upperPlotBand->to = $maxinvertal;
                array_push($plotBands, $upperPlotBand);

                $yAxis->plotBands = $plotBands;

                // ngitung YRD, MRA, dan latest period

                // latest period
                $maxdata = DB::select("
                    SELECT f_getkpicolor(value, targetmax, targetmed, inverted) as kpicolor
                    FROM kpiheader h, kpiline l, kpimaster k 
                    where h.id = l.kpiheaderid 
                    and l.kpiid = k.id
                    and h.apgroup in (" . $apgroup . ") and l.kpiid = ? and h.wfstatus = 'SUBMITTED'
                    order by h.updated_at desc
                    limit 0, 1;
                ", [$kpi->id]);
                if(count($maxdata) > 0)
                    $chartOption->currentIndicator = $maxdata[0]->kpicolor;

                // year to date
                $ytddata = DB::select("
                    SELECT f_getkpicolor(a.value, a.targetmax, a.targetmed, a.inverted) as kpicolor
                    FROM (
                    select avg(value) as value, max(targetmax) as targetmax, max(targetmed) as targetmed, max(inverted) as inverted
                    FROM kpiheader h, kpiline l, kpimaster k 
                    where h.id = l.kpiheaderid 
                    and l.kpiid = k.id
                    and h.apgroup in (" . $apgroup . ") and l.kpiid = ? and h.wfstatus = 'SUBMITTED'
                    and str_to_date(CONCAT('01-', h.period), '%d-%b-%Y') >= CONCAT(DATE_FORMAT(NOW(), '%Y'), '-01-01')
                    ) as a
                ", [$kpi->id]);
                if(count($ytddata) > 0)
                    $chartOption->ytdIndicator = $ytddata[0]->kpicolor;

                $oneyeardata = DB::select("
                    SELECT f_getkpicolor(a.value, a.targetmax, a.targetmed, a.inverted) as kpicolor
                    FROM (
                    select avg(value) as value, max(targetmax) as targetmax, max(targetmed) as targetmed, max(inverted) as inverted
                    FROM kpiheader h, kpiline l, kpimaster k 
                    where h.id = l.kpiheaderid 
                    and l.kpiid = k.id
                    and h.apgroup in (" . $apgroup . ") and l.kpiid = ? and h.wfstatus = 'SUBMITTED'
                    and str_to_date(CONCAT('01-', h.period), '%d-%b-%Y') >= DATE_SUB(CURDATE(), INTERVAL 1 YEAR)
                    ) as a
                ", [$kpi->id]);
                if(count($oneyeardata) > 0)
                    $chartOption->oneyearIndicator = $oneyeardata[0]->kpicolor;
            }
            
            $chartOption->yAxis = $yAxis;

            array_push($chartoptions, $chartOption);
        }

        // Log::info(json_encode($chartoptions));

        return response()->json([
            'status' => 'SUCCESS',
            'periods' => $availableperiods,
            'chartoptions' => $chartoptions,
            'groupap' => $groupAp
        ]);
    }

    public function kpisubmit(Request $request)
    {
        $kpiheader = KpiHeader::where('apgroup', Auth::user()->groupap)
                        ->where('period', $request->period)
                        ->first();
        $kpiheader->wfstatus = 'SUBMITTED';
        $kpiheader->save();
        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function kpiuploadfile(Request $request)
    {
        if($request->file) {
            // Log::info(print_r($request->file, true));
            $kpiheader = KpiHeader::firstOrNew([
                'apgroup' => Auth::user()->groupap,
                'period' => $request->period
            ]);
            $kpiheader->save();

            // get extension
            $filename = $request->name . '-' .  time() . '.' . $request->file->getClientOriginalExtension();

            $request->file->storeAs('kpi/' . Auth::user()->groupap . '-' . $request->period, $filename);

            // simpan di database
            $doc = Documents::create([
                'owner' => 'KPIHEADER',
                'ownerid' => $kpiheader->id,
                'category' => $request->name,
                'docname' => $filename
            ]);

            $doclist = DB::select("
                select vl.name as name, vl.description as description, d.docname, d.id from valuelist vl left join phidocuments d on d.category = vl.name and d.owner = 'KPIHEADER' and d.ownerid = ? 
                where vl.category = 'KPIATTACHMENT' 
                order by vl.id
            ", [$kpiheader->id]);

            // $doclist = Documents::where('owner', 'VENDOR')
            //                 ->where('ownerid', $request->vendorid)
            //                 ->get();

            return response()->json([
                'status' => 'SUCCESS',
                'doclist' => $doclist
            ]);
        }
    }

    public function kpiheaderupdate(Request $request)
    {
        $kpiheader = KpiHeader::firstOrNew([
            'apgroup' => Auth::user()->groupap,
            'period' => $request->period
        ]);
        $kpiheader->update([
            "$request->category" => $request->value
        ]);
        $kpiheader->save();

        return response()->json([
            'status' => 'SUCCESS',
            'kpi' => $kpiheader
        ]);
    }

    public function kpiupdate(Request $request)
    {
        // cari headernya dulu, kalo ndak create new header
        $kpiheader = KpiHeader::firstOrNew([
            'apgroup' => Auth::user()->groupap,
            'period' => $request->period
        ]);
        $kpiheader->save();

        $kpiline = KpiLine::firstOrNew([
            'kpiheaderid' => $kpiheader->id,
            'kpiid' => $request->kpiid,
        ]);
        $kpiline->value = $request->value;
        $kpiline->save();

        return response()->json([
            'status' => 'SUCCESS',
        ]);
    }

    public function kpilist($period)
    {
        $kpiheader = KpiHeader::firstOrNew([
            'apgroup' => Auth::user()->groupap,
            'period' => $period
        ]);
        $kpiheader->save();

        $kpilist = DB::select("
            select m.id, m.name, m.description, kpiline.value from kpimaster m left join kpiline 
            on m.id = kpiline.kpiid
            and kpiline.kpiheaderid = ? 
            order by m.id
        ", [$kpiheader->id]);

        $kpiattachmentlist = DB::select("
            select vl.name as name, vl.description as description, d.docname, d.id from valuelist vl 
                left join phidocuments d on d.category = vl.name and d.owner = 'KPIHEADER' and d.ownerid = ? 
            where vl.category = 'KPIATTACHMENT' 
            order by vl.id
        ", [$kpiheader->id]);

        return response()->json([
            'status' => 'SUCCESS',
            'kpilist' => $kpilist,
            'kpiheader' => $kpiheader,
            'attachmentlist' => $kpiattachmentlist,
        ]);
    }

    public function getDepartments()
    {
        $deplist = Department::orderby('name')->get();
        $user = User::with(['department'])
                    ->where('id', Auth::id())
                    ->first();
        return response()->json([
            'status' => 'SUCCESS',
            'departmentlist' => $deplist,
            'me' => $user,
        ]);
    }

    public function getReports($rfqid)
    {
        $reportserver = 'http://10.254.200.130:8080/birt';

        $reports = DB::select("
                SELECT * FROM reports
            ");
        return response()->json([
            'status' => 'SUCCESS',
            'reports' => $reports,
            'reportserver' => $reportserver,
        ]);
    }
    
    public function getBuyer()
    {
        $userrole = UserRole::with(['user'])->where('role', 'BUYER')
                        // ->orderby('users.name')
                        ->get();
        return response()->json([
            'status' => 'SUCCESS',
            'userrole' => $userrole,
        ]);
    }

        

    public function loadVendorByCommodity(Request $request)
    {
        $vendorlist = DB::select("
            SELECT (select golusaha from vendor where vendorid = phivendor.sapid) as golusaha, phivendor.sapid as id, phivendor.name, vendor_letter.lettertype, vendor_letter.aktapendirianno FROM phivendor left join vendor_letter on phivendor.sapid = vendor_letter.vendorid and vendor_letter.lettertype = '01'
            where sapid in (select vendorid from vendor_commodities where commid = ?)
        ", [$request->commid]);

        if(count($vendorlist) == 0) {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'Tidak ada vendor dengan bidang/subbidang ini'
            ]);
        }

        // delete all bidderlist
        RfqLine::where('rfqid', $request->rfqid)->delete();

        // masukin ke rfqline
        $bidderlist = array();
        foreach($vendorlist as $vendor) {
            $vendortype = (is_null($vendor->lettertype)) ? 'NSKT' : 'VSKT';
            $rfqline = RfqLine::create([
                'rfqid' => $request->rfqid,
                'vendorname' => $vendor->name,
                'vendortype' => $vendortype,
                'sktnum' => $vendor->aktapendirianno,
                'golusaha' => $vendor->golusaha == null ? '-' : $vendor->golusaha,
                'sanksi' => '-',
                'exp_contracttitle' => '-',
                'exp_companies' => '-',
                'remarks' => '-',
                'vendorid' => $vendor->id
            ]);
            $bidderlist[] = $rfqline;
        }

        return response()->json([
            'status' => 'SUCCESS',
            'bidderlist' => $bidderlist
        ]);
    }

    public function vendorByName(Request $request) {
        $vendorlist = DB::select("
            SELECT p.sapid as vendorid, p.name, vl.lettertype, vl.aktapendirianno, v.golusaha FROM phivendor p, vendor v
            left join vendor_letter vl on v.vendorid = vl.vendorid and vl.lettertype = '01'
            where p.sapid != 'NA'
            and p.sapid = v.vendorid
            and p.name like '%$request->term%'
        ");

        return response()->json([
            'status' => 'SUCCESS',
            'vendorlist' => $vendorlist
        ]);
    }

    public function vendorById($vendorid)
    {
        $vendor = DB::select("
            SELECT name, aktapendirianno FROM vendor v
            left join vendor_letter vl on v.vendorid = vl.vendorid and vl.lettertype = '01'
            where v.vendorid = ?
        ", [$vendorid]);

        $retval = $vendor;
        if(count($vendor) > 0) {
            $retval = $vendor[0];
        }

        return response()->json([
            'status' => 'SUCCESS',
            'vendor' => $retval
        ]);
    }

    public function updateVerificationList(Request $request)
    {
        foreach($request->verifications as $v) {
            $verification = VendorVerification::firstOrNew([
                'vendorid' => $request->vendorid,
                'category' => $v['name']
            ]);
            $verification->result = $v['result'];
            $verification->remarks = $v['remarks'];
            $verification->save();
        }
        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function updateAdminEval(Request $request)
    {
        foreach($request->adminevallist as $eval) {
            $adminEval = BidEval::firstOrNew([
                'rfqid' => $request->rfqid,
                'vendorid' => $request->vendorid,
                'evaladminno' => $eval['name'],
            ]);
            $adminEval->evaladminresult =  $eval['evaladminresult'] == null ? 'N/A' : $eval['evaladminresult'];
            $adminEval->save();
        }

        $overall = BidEval::firstOrNew([
            'rfqid' => $request->rfqid,
            'vendorid' => $request->vendorid,
            'evaladminno' => 'OVERALL',
        ]);
        $overall->evaladminresult = $request->recommendation;
        $overall->remarks = $request->remarks;
        $overall->save();

        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function updateCommercialEval(Request $request)
    {
        $commercialEval = TechnicalEval::firstOrNew([
            'rfqid' => $request->rfqid,
            'vendorid' => $request->vendorid
        ]);

        // kalo winner = 1, semua harus direset dulu
        if($request->winner == 1) {
            TechnicalEval::where('rfqid', $request->rfqid)
                ->update([
                    'winner' => 0,
                ]);
        }

        $commercialEval->hasilkomersial = $request->result == null ? 'TIDAK LULUS' : 'LULUS';
        $commercialEval->keterangankomersial = $request->remarks;
        $commercialEval->winner = $request->winner;
        $commercialEval->quoteprice = $request->quoteprice;
        $commercialEval->afternegoprice = $request->afternegoprice;
        $commercialEval->save();

        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function updateTechnicalEval(Request $request)
    {
        $technicaleval = TechnicalEval::firstOrNew([
            'rfqid' => $request->rfqid,
            'vendorid' => $request->vendorid
        ]);
        
        $technicaleval->hasil = $request->result == null ? 'TIDAK LULUS' : 'LULUS';
        $technicaleval->keterangan = $request->remarks;
        $technicaleval->save();

        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function commercialEval(Request $request)
    {
        $eval = DB::table('technicaleval')
                     ->leftJoin('vendor', 'technicaleval.vendorid', '=', 'vendor.vendorid')
                    ->where('technicaleval.rfqid', $request->rfqid)
                    ->where('technicaleval.vendorid', $request->vendorid)
                    ->first();
        
        return response()->json([
            'status' => 'SUCCESS',
            'eval' => $eval
        ]);
    }

    public function technicalEval(Request $request)
    {
        $eval = DB::table('bideval')
                    ->leftJoin('technicaleval', function($join) {
                        $join->on('technicaleval.rfqid', '=', 'bideval.rfqid')
                             ->on('technicaleval.vendorid', '=', 'bideval.vendorid');
                    })
                    ->leftJoin('vendor', 'bideval.vendorid', '=', 'vendor.vendorid')
                    ->where('bideval.rfqid', $request->rfqid)
                    ->where('bideval.vendorid', $request->vendorid)
                    ->where('bideval.evaladminno', 'OVERALL')
                    ->first();
        
        return response()->json([
            'status' => 'SUCCESS',
            'eval' => $eval
        ]);
    }

    public function vendorVerification($vendorid)
    {
        $vv = DB::select("
            SELECT * FROM valuelist vl
              left join vendorverification vv
              on vl.name = vv.category
              and vv.vendorid = ?
            where vl.category = 'VENDORVERIFY'
            and vl.name != 'OVERALL'
        ", [$vendorid]);

        $overall = VendorVerification::where('vendorid', $vendorid)
                    ->where('category', 'OVERALL')
                    ->first();
        
        return response()->json([
            'status' => 'SUCCESS',
            'verifications' => $vv,
            'overall' => $overall
        ]);
    }

    public function adminEvalList(Request $request)
    {
        $adminevallist = DB::select("
            SELECT * FROM valuelist vl 
                left join bideval b 
                on vl.name = b.evaladminno 
                and b.rfqid = ? 
                and b.vendorid = ? 
            where vl.category = 'ADMINEVAL'
            and vl.name != 'OVERALL'
        ", [$request->rfqid, $request->vendorid]);

        // overall
        $overall = BidEval::where('rfqid', $request->rfqid)
                    ->where('vendorid', $request->vendorid)
                    ->where('evaladminno', 'OVERALL')
                    ->first();
        
        $recommendation = ($overall == null) ? 0 : $overall->evaladminresult;

        return response()->json([
            'status' => 'SUCCESS',
            'adminevallist' => $adminevallist,
            'recommendation' => $recommendation,
            'adminevalremarks' => $overall->remarks,
        ]);
    }

    public function getProcurement($rfqid)
    {
        $rfq = Rfq::with(['rfqline', 'buyer', 'doclist'])
                ->where('id', $rfqid)
                ->first();

        $wfapproval = WfApproval::where('ownerid', $rfqid)
                        ->where('ownertable', 'RFQ')
                        ->select(DB::raw("id, wfname, status, instanceid, ownerid, wfassignee, (select name from users where SUBSTRING_INDEX(email, '@', 1) = TRIM(wfassignee)) as assignee "))
                        ->whereNotIn('status', ['APPR'])
                        ->first();
        
        return response()->json([
            'status' => 'SUCCESS',
            'rfq' => $rfq,
            'wfapproval' => $wfapproval
        ]);
    }

    public function getProcurementList()
    {
        $rfqlist = Rfq::select(DB::raw("
            id, rfqnum, title, rfqtype, proctype,
            procmethod, prnum, documentdate,
            prdate, (select name from users where users.email = concat(rfq.buyer, '@pertamina.com')) as buyer, procmgr, procvp,
            wftype, wfstatus, createdby, created_at, updated_at
            "))->orderby('created_at', 'desc')->get()->take(100);

        return response()->json([
            'status' => 'SUCCESS',
            'rfqlist' => $rfqlist,
        ]);
    }

    public function baepList($rfqid)
    {
        $baepList = Baep::where('rfqid', $rfqid)
                     ->get();
        
        // bidder list
        $bidderlist = RfqLine::where('rfqid', $rfqid)->get();

        // rfq
        $rfq = Rfq::find($rfqid);

        // procmethod
        $procmethod2 = ValueList::where('name', $rfq->procmethod)
                        ->first();    
        
        // evaluasi
        $bideval = DB::select("
        SELECT b.rfqid, b.vendorid, 
            (select name from vendor v where v.vendorid = b.vendorid) vendorname,
            b.evaladminresult,
            t.hasil hasilteknis,
            t.keterangan keteranganteknis,
            t.hasilkomersial,
            t.keterangankomersial,
            t.winner,
            t.quoteprice,
            t.afternegoprice,
            format(t.quoteprice / rfq.ownerestimate * 100, 2, 'id_ID') variance,
            format(t.afternegoprice / rfq.ownerestimate * 100, 2, 'id_ID') negovariance
        FROM rfq, bideval b
            left join technicaleval t
            on b.rfqid = t.rfqid
            and b.vendorid = t.vendorid
        where b.rfqid = ?
        and b.evaladminno = 'OVERALL'
        and b.rfqid = rfq.id
        ", [$rfqid]);

        return response()->json([
            'status' => 'SUCCESS',
            'baeplist' => $baepList,
            'bidderlist' => $bidderlist,
            'rfq' => $rfq,
            'procmethod2' => $procmethod2,
            'bideval' => $bideval
        ]);
    }

    public function updateBaep(Request $request)
    {
        $baep = Baep::firstOrNew([
            'rfqid' => $request->rfqid,
            'section' => $request->section,
            'order' => $request->order
        ]);
        $baep->content = $request->content == null ? '' : $request->content;
        $baep->save();

        return response()->json([
            'status' => 'SUCCESS',
            'baep' => $baep
        ]);
    }

    public function updateKelengkapanDokumen(Request $request)
    {
        $rfqline = RfqLine::where('id', $request->rfqlineid)->first();
        $rfqline->kelengkapandokumen = $request->kelengkapandokumen;
        $rfqline->save();

        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function updateAmbilDokumen(Request $request)
    {
        $rfqline = RfqLine::where('id', $request->rfqlineid)->first();
        $rfqline->ambildokumen = $request->ambildokumen;
        $rfqline->save();

        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function deleteProcurement($bidderid)
    {
        RfqLine::where('id', $bidderid)->delete();

        return response()->json([
            'status' => 'SUCCESS',
            'message' => 'Data berhasil dihapus'
        ]);
    }

    public function getRfqMilestone($rfqid)
    {
        $milestone = DB::select("
            select vl.name, vl.description, rm.aktivitas, rm.kodeaktivitas, rm.startdate, rm.enddate 
            from valuelist vl
            left join rfqmilestone rm
            on rm.rfqid = ?
            and rm.aktivitas = vl.description
            where vl.category = 'BIDACTIVITY'
            order by vl.name
        ", [$rfqid]);

        return response()->json([
            'status' => 'SUCCESS',
            'milestone' => $milestone
        ]);
    }

    public function getFungsiPenggunaList()
    {
        $valuelist = ValueList::where('category', 'DEPARTMENT')
                        ->orderby('name', 'asc')
                        ->get();
        return response()->json([
            'status' => 'SUCCESS',
            'fungsipengguna' => $valuelist
        ]);
    }

    public function updateProcurement(Request $request)
    {
        $rfq = Rfq::find($request->rfqid);
        $rfq->title = $request->title;
        $rfq->rfqtype = $request->rfqtype;
        $rfq->proctype = $request->proctype;
        $rfq->procmethod = $request->procmethod;
        $rfq->prnum = $request->prnum;
        $rfq->documentdate = $request->documentdate;
        $rfq->prdate = $request->prdate;
        $rfq->bidopeningdate = $request->bidopeningdate;
        $rfq->bidopeningdate2 = $request->bidopeningdate2;
        $rfq->prebiddate = $request->prebiddate;
        $rfq->invitationdate = $request->invitationdate;
        $rfq->ambildokumendate = $request->ambildokumendate;
        $rfq->invitetobidletternum = $request->invitetobidletternum; 
        $rfq->bidopeningletternum = $request->bidopeningletternum;
        $rfq->memoadminevalletternum = $request->memoadminevalletternum; 
        $rfq->technicalevalletternum = $request->technicalevalletternum; 
        $rfq->evalresultletternum = $request->evalresultletternum;
        $rfq->awardletternum = $request->awardletternum;
        $rfq->currencycode = $request->currencycode;
        $rfq->ownerestimate = $request->ownerestimate;
        $rfq->sistemevaluasipenawaran = $request->sistemevaluasipenawaran;
        $rfq->periode = $request->periode;
        $rfq->justifikasimetode = $request->justifikasimetode;
        $rfq->sampul = $request->sampul;
        $rfq->tkdn = $request->tkdn;
        $rfq->fungsipengguna = $request->fungsipengguna;
        $rfq->estminvalue = $request->estminvalue;
        $rfq->estmaxvalue = $request->estmaxvalue;
        $rfq->save();

        foreach($request->bidderlist as $bd) {
            if(isset($bd['id'])) {
                RfqLine::where('id', $bd['id'])->update([
                    'vendorname' => $bd['vendorname'],
                    'vendortype' => $bd['vendortype'],
                    'sktnum' => $bd['sktnum'],
                    'golusaha' => '-', // $bd['golusaha'],
                    'sanksi' => $bd['sanksi'],
                    'exp_contracttitle' => $bd['exp_contracttitle'],
                    'exp_companies' => $bd['exp_companies'],
                    'remarks' => $bd['remarks'],
                    'vendorid' => $bd['vendorid']
                ]);
            }
            else {
                RfqLine::create([
                    'rfqid' => $rfq->id,
                    'vendorname' => $bd['vendorname'],
                    'vendortype' => $bd['vendortype'],
                    'sktnum' => $bd['sktnum'],
                    'golusaha' => '-', // $bd['golusaha'],
                    'sanksi' => $bd['sanksi'],
                    'exp_contracttitle' => $bd['exp_contracttitle'],
                    'exp_companies' => $bd['exp_companies'],
                    'remarks' => $bd['remarks'],
                    'vendorid' => $bd['vendorid']
                ]);
            }
        }

        // loop -- milestone
        foreach($request->milestone as $milestone) {
            $rfqm = RfqMilestone::firstOrNew([
                'rfqid' => $rfq->id,
                'kodeaktivitas' => $milestone['name'],
            ]);
            $rfqm->kodeaktivitas = $milestone['name'];
            $rfqm->aktivitas = $milestone['description'];
            $rfqm->startdate = $milestone['startdate'];
            $rfqm->enddate = $milestone['enddate'];
            $rfqm->save();
        }

        return response()->json([
            'status' => 'SUCCESS',
            'rfq' => $rfq,
            'rfqline' => RfqLine::where('rfqid', $rfq->id)->get()
        ]);
    }

    public function saveNewProcurement(Request $request)
    {
        // cari proc mgr dan proc vp
        $scmManager = UserRole::with(['user'])
                        ->where('role', 'PROCMGR')
                        ->first();
        $scmManagerEmail = $scmManager['user']['email'];
        $explodedEmail = explode('@', $scmManagerEmail);
        $procmgr = $explodedEmail[0];

        $scmVp = UserRole::with(['user'])
                    ->where('role', 'PROCVP')
                    ->first();
        $scmVpEmail = $scmVp['user']['email'];
        $explodedEmail = explode('@', $scmVpEmail);
        $procvp = $explodedEmail[0];

        $rfq = Rfq::create([
            'rfqnum' => $request->rfqnum,
            'title' => $request->title,
            'rfqtype' => $request->rfqtype,
            'proctype' => $request->proctype,
            'procmethod' => $request->procmethod,
            'prnum' => $request->prnum,
            'documentdate' => $request->documentdate,
            'prdate' => $request->prdate,
            'buyer' => Auth::user()->ldapname,
            'buyerid' => Auth::user()->id,
            'procmgr' => $procmgr,
            'procvp' => $procvp,
            'createdby' => Auth::user()->id,
            'bidopeningdate' => $request->bidopeningdate,
            'bidopeningdate2' => $request->bidopeningdate2,
            'prebiddate' => $request->prebiddate,
            'invitationdate' => $request->invitationdate,
            'ambildokumendate' => $request->ambildokumendate,
            'invitetobidletternum' => $request->invitetobidletternum, 
            'bidopeningletternum' => $request->bidopeningletternum,
            'memoadminevalletternum' => $request->memoadminevalletternum, 
            'technicalevalletternum' => $request->technicalevalletternum, 
            'evalresultletternum' => $request->evalresultletternum,
            'awardletternum' => $request->awardletternum,
            'currencycode' => $request->currencycode,
            'ownerestimate' => $request->ownerestimate,
            'sistemevaluasipenawaran' => $request->sistemevaluasipenawaran,
            'periode' => $request->periode,
            'justifikasimetode' => $request->justifikasimetode,
            'sampul' => $request->sampul,
            'tkdn' => $request->tkdn,
            'fungsipengguna' => $request->fungsipengguna,
            'estminvalue' => $request->estminvalue,
            'estmaxvalue' => $request->estmaxvalue
        ]);

        // loop
        foreach($request->bidderlist as $bidderlist) {
            RfqLine::create([
                'rfqid' => $rfq->id,
                'vendorname' => $bidderlist['vendorname'],
                'vendortype' => $bidderlist['vendortype'],
                'sktnum' => $bidderlist['sktnum'],
                'golusaha' => '-', //$bidderlist['golusaha'],
                'sanksi' => $bidderlist['sanksi'],
                'exp_contracttitle' => $bidderlist['exp_contracttitle'],
                'exp_companies' => $bidderlist['exp_companies'],
                'remarks' => $bidderlist['remarks'],
                'vendorid' => $bidderlist['vendorid']
            ]);
        }

        // loop -- milestone
        foreach($request->milestone as $milestone) {
            $rfqm = RfqMilestone::firstOrNew([
                'rfqid' => $rfq->id,
                'kodeaktivitas' => $milestone['name'],
            ]);
            $rfqm->kodeaktivitas = $milestone['name'];
            $rfqm->aktivitas = $milestone['description'];
            $rfqm->startdate = $milestone['startdate'];
            $rfqm->enddate = $milestone['enddate'];
            $rfqm->save();
        }

        return response()->json([
            'status' => 'SUCCESS',
            'rfq' => $rfq,
            'rfqline' => RfqLine::where('rfqid', $rfq->id)->get(),
        ]);

    }

    public function updateLetter(Request $request)
    {
        $vletter = VendorLetter::find($request->letterid);
        $vletter->sktvaliditydate = $request->masaberlakudate;
        $vletter->aktapendirianno = $request->nomorsurat;
        $vletter->localupdated = 1;
        $vletter->save();

        $letterlist = VendorLetter::where('vendorid', $request->vendorid)->get();

        return response()->json([
            'status' => 'SUCCESS',
            'letterlist' => $letterlist
        ]);
    }

    public function deleteRfqAttachment(Request $request)
    {
        $doc = Documents::find($request->attachmentid);
        $ownerid = $doc->ownerid;

        Storage::delete('rfq/' . $ownerid . '/' . $doc->docname);
        $doc->delete();

        $attachmentlist = DB::select("
            SELECT * FROM phidocuments
            where owner = 'RFQ' and ownerid = ?
            order by docname
        ", [$ownerid]);

        // $attachmentlist = Documents::where('owner', 'VENDOR')
        //     ->where('ownerid', $ownerid)
        //     ->get();
        
        return response()->json([
            'status' => 'SUCCESS',
            'attachmentlist' => $attachmentlist
        ]);
    }

    public function deleteAttachment(Request $request)
    {
        $doc = Documents::find($request->attachmentid);
        $ownerid = $doc->ownerid;

        Storage::delete('vendor/' . $ownerid . '/' . $doc->docname);
        $doc->delete();

        $attachmentlist = DB::select("
            select vl.name as vlname, vl.description as category, d.docname, d.id from valuelist vl left join phidocuments d on d.category = vl.name and d.owner = 'VENDOR' and d.ownerid = ? 
            where vl.category = 'ATTACHMENT' 
            order by vl.description
        ", [$ownerid]);

        // $attachmentlist = Documents::where('owner', 'VENDOR')
        //     ->where('ownerid', $ownerid)
        //     ->get();
        
        return response()->json([
            'status' => 'SUCCESS',
            'attachmentlist' => $attachmentlist
        ]);
    }

    public function deleteCommodity(Request $request)
    {
        VendorCommodities::find($request->commodityid)->delete();

        $commoditylist = $commlist = DB::select("
                SELECT distinct id, commid, description as description FROM vendor_commodities where vendorid = ?
            ", [$request->vendorid]
        );

        return response()->json([
            'status' => 'SUCCESS',
            'commoditylist' => $commoditylist,
        ]);
    }

    public function saveCommodity(Request $request)
    {
        VendorCommodities::create([
            'clientid' => '180',
            'vendorid' => $request->vendorid,
            'commid' => $request->commid,
            'description' => $request->commdescription,
            'localupdated' => 1,
        ]);

        $commoditylist = $commlist = DB::select("
                SELECT distinct id, commid, description as description FROM vendor_commodities where vendorid = ?
            ", [$request->vendorid]
        );
        
        return response()->json([
            'status' => 'SUCCESS',
            'commoditylist' => $commoditylist
        ]);
    }

    public function saveNewLetter(Request $request)
    {
        // ambil description dari valuelist 
        $description = DB::select("
            select description from valuelist where category = 'LETTER'
            and name = ?
        ", [$request->lettertype]);

        VendorLetter::create([
            'vendorid' => $request->vendorid,
            'lettertype' => $request->lettertype,
            'description' => $description[0]->description,
            'sktvaliditydate' => $request->masaberlakudate,
            'aktapendirianno' => $request->nomorsurat,
            'localupdated' => 1,
        ]);

        $letterlist = VendorLetter::where('vendorid', $request->vendorid)->get();

        return response()->json([
            'status' => 'SUCCESS',
            'letterlist' => $letterlist
        ]);
    }

    public function commodityListVendor()
    {
        $commlist = DB::select("
            SELECT distinct commid, upper(description) as description FROM vendor_commodities 
            where vendorid in (select sapid from phivendor)
            order by commid
        ");

        return response()->json([
            'status' => 'SUCCESS',
            'commoditylist' => $commlist
        ]);
    }

    public function commodityList()
    {
        $commlist = DB::select("
            SELECT name as commid, description FROM valuelist WHERE category = 'COMMODITIES'
        ");

        return response()->json([
            'status' => 'SUCCESS',
            'commoditylist' => $commlist
        ]);
    }

    public function attachmentList()
    {
        $attachmentlist = DB::select("
            select name, description from valuelist where category = 'ATTACHMENT' 
            order by name asc
        ");

        return response()->json([
            'status' => 'SUCCESS',
            'attachmentlist' => $attachmentlist
        ]);
    }

    public function unavailableLetter($vendorid)
    {
        $letterlist = DB::select("
            select name, description from valuelist where category = 'LETTER' 
            and name not in (select lettertype from vendor_letter where vendorid = ? )
            order by description
        ", [$vendorid]);

        return response()->json([
            'status' => 'SUCCESS',
            'letterlist' => $letterlist
        ]);
    }

    public function updateVendor(Request $request)
    {
        // update datanya di SAPVendor
        // kalau vendorid: NA --> perlu bikin baru dg ID PHI+ID --> lalu update referensinya di tabel PHIVENDOR
        // kalau vendorid ada tinggal update

        // ruwet yo le, hehehehe.... (30 Januari 2019)
        $sapvendor = null;
        $phivendor = ($request->flag == 'UPDATEVENDOR') ? Vendor::find($request->phivendorid) : new Vendor();

        if($request->vendorid != 'NA') {
            $sapvendor = SapVendor::where('vendorid', $request->vendorid)->first();
        }
        else {
            $uniqueid = 'PHI' . time();
            $sapvendor = new SapVendor();
            $sapvendor->clientid = '180';
            $sapvendor->vendorid = $uniqueid;
            $phivendor->sapid = $uniqueid;

            if($request->flag == 'NEWVENDOR') {
                $phivendor->civdid = 'NA';
                $phivendor->ownerid = 'PHI';
		$phivendor->name = $request->name;
		$phivendor->verified = '0';
            }
        }

        // update phivendor
        if($request->source == 'VENDORVERIFY') {
            $phivendor->verified = $request->verified;
            $phivendor->verifieddate = $request->verifieddate;
        }

        // update sapvendor
        $sapvendor->name = $request->name;
        $sapvendor->street = $request->address;
        $sapvendor->city = $request->city;
        $sapvendor->district = $request->district;
        $sapvendor->taxnumber = $request->npwp;
        $sapvendor->telephone1 = $request->telephone1;
        $sapvendor->telephone2 = $request->telephone2;
        $sapvendor->faxnumber = $request->faxnumber;
        $sapvendor->email = $request->email;
        $sapvendor->bankname = $request->bankname;
        $sapvendor->bankaccountno = $request->bankaccountnumber;
        $sapvendor->bankname2 = $request->bankname2;
        $sapvendor->bankaccountno2 = $request->bankaccountnumber2;
        $sapvendor->golusaha = $request->golusaha;
        $sapvendor->csmsscore = $request->csmsscore;
        $sapvendor->localupdated = 1;

        // save sapvendor dan phivendor
        $sapvendor->save();
        $phivendor->save();


        return response()->json([
            'status' => 'SUCCESS',
            'vendor' => $phivendor
        ]);
    }

    public function uploadRfqFile(Request $request)
    {
        if($request->file) {
            // get extension
            $filename = $request->rfqid . '-' .  time() . '.' . $request->file->extension();

            $request->file->storeAs('rfq/' . $request->rfqid, $filename);

            // simpan di database
            $doc = Documents::create([
                'owner' => 'RFQ',
                'ownerid' => $request->rfqid,
                'category' => $request->filename,
                'docname' => $filename
            ]);

            $doclist = DB::select("
                SELECT * FROM phidocuments
                where owner = 'RFQ' and ownerid = ?
                order by docname
            ", [$request->rfqid]);

            return response()->json([
                'status' => 'SUCCESS',
                'doclist' => $doclist
            ]);
        }

        return response()->json([
            'status' => 'ERROR',
            'message' => 'Cannot store attachments'
        ]);
    }

    public function uploadVendorFile(Request $request)
    {
        if($request->file) {
            // get extension
            $filename = $request->vendorid . '-' .  time() . '.' . $request->file->extension();

            $request->file->storeAs('vendor/' . $request->vendorid, $filename);

            // simpan di database
            $doc = Documents::create([
                'owner' => 'VENDOR',
                'ownerid' => $request->vendorid,
                'category' => $request->filename,
                'docname' => $filename
            ]);

            $doclist = DB::select("
                select vl.name as vlname, vl.description as category, d.docname, d.id from valuelist vl left join phidocuments d on d.category = vl.name and d.owner = 'VENDOR' and d.ownerid = ? 
                where vl.category = 'ATTACHMENT' 
                order by vl.description
            ", [$request->vendorid]);

            // $doclist = Documents::where('owner', 'VENDOR')
            //                 ->where('ownerid', $request->vendorid)
            //                 ->get();

            return response()->json([
                'status' => 'SUCCESS',
                'doclist' => $doclist
            ]);
        }

        return response()->json([
            'status' => 'ERROR',
            'message' => 'Cannot store attachments'
        ]);

        // return $request->all();
    }

    public function vendorDetail($vendorid)
    {
        $vendor = Vendor::find($vendorid);
        
        // attachments
        $attachments = DB::select("
            select vl.name vlname, vl.description as category, d.docname, d.id from valuelist vl left join phidocuments d on d.category = vl.name and d.owner = 'VENDOR' and d.ownerid = ? 
            where vl.category = 'ATTACHMENT' 
            order by vl.description
        ", [$vendorid]);

        // $attachments = Documents::where('owner', 'VENDOR')
        //                 ->where('ownerid', $vendor->id)
        //                 ->get();

        if($vendor->sapid != 'NA') {
            $sapvendor = SapVendor::with(['commodities', 'letters'])
                    ->where('vendorid', $vendor->sapid)
                    ->first();
            
            return response()->json([
                'status' => 'SUCCESS',
                'vendor' => $sapvendor,
                'attachments' => $attachments,
                'phivendorid' => $vendor->id,
                'verified' => $vendor->verified,
                'verifieddate' => $vendor->verifieddate,
            ]);
        }
        
        return response()->json([
            'status' => 'SUCCESS',
            'vendor' => $vendor,
            'attachments' => $attachments,
            'phivendorid' => $vendor->id,
        ]);
    }

    public function filterVendor(Request $request)
    {
        $where = '';
        if(isset($request->verified)) {
            $where .= ($request->verified == 'VERIFIED') ? "
                id in (select ownerid from phidocuments where owner = 'VENDOR')
            " : "1 = 1";
        }
        $vendorList = DB::select("
            select * from phivendor
            where $where
            order by name asc
        ");

        return response()->json([
            'status' => 'SUCCESS',
            'vendorlist' => $vendorList,
        ]);
    }

    public function vendorList()
    {
        $vendorList = Vendor::orderby('name', 'asc')->get()->take(1000);
        return response()->json([
            'status' => 'SUCCESS',
            'vendorlist' => $vendorList
        ]);
        // return 'hehe';
    }

    public function getBidControl(Request $request)
    {
        $bidcontrol = BidControl::orderby('created_at', 'desc')->get();
        return response()->json([
            'status' => 'SUCCESS',
            'data' => $bidcontrol,
        ]);
    }

    public function saveBidControl(Request $request)
    {
        BidControl::create([
            'rfqnum' => $request->rfqnum,
            'title' => $request->title, 
            'department' => $request->department,
            'openingdate' => $request->openingdate,
            'closingdate' => $request->closingdate,  
            'submitdate' => $request->submitdate,
            'bidopeningdate' => $request->bidopeningdate, 
            'meetingdate' => $request->meetingdate, 
            'vendorinvited' => $request->vendorinvited, 
            'vendorrejected' => $request->vendorrejected, 
            'vendorsubmitted' => $request->vendorsubmitted,
            'vendorsubmittedontime' => $request->vendorsubmittedontime, 
            'vendorsubmittedlate' => $request->vendorsubmittedlate, 
            'vendornotsubmitted' => $request->vendornotsubmitted, 
            'routestatus' => 'DRAFT',
            'buyer' => $request->buyer,
        ]);

        return response()->json([
            'status' => 'SUCCESS',
            'message' => 'New bid control created'
        ]);
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        if(Hash::check($request->password, $user->password)) {
            // update new password
            $user->password = Hash::make($request->newpassword);
            $user->save();

            return response()->json([
                'status' => 'SUCCESS',
                'message' => 'Password updated. Logging you out...'
            ]);
        }

        return response()->json([
            'status' => 'ERROR',
            'message' => 'Cannot update password'
        ]);

    }

    public function register(Request $request)
    {
        // validate input
        $validator = $this->validateInput($request->input('data'));

        if($validator->fails()) {
            return response()->json([
                'status' => 'ERROR',
                'message' => $validator->errors()]
            );
        }

        // register usernya!
        $data = $request->input('data');
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'departmentid' => $data['department']
        ]);

        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40),
        ]);

        Mail::to($data['email'])->bcc(['galih.satriaji@pertamina.com'])->queue(new VerifyMail($user));

        return response()->json([
            'status' => 'SUCCESS',
            'message' => 'User registered sucessfully. Please activate your user by click confirmation link sent to your email'
        ]);
    }

    public function login(Request $request)
    {
        $user = User::with(['authorizations'])->where('email', $request->email)->first();
        $MINIMUM_VERSION = '0.0.5';
    	if($user) {
    		if(Hash::check($request->password, $user->password)) {
    			$token = $user->createToken('Grant Client')->accessToken;
    			return response()->json([
    				'status' => 'GRANTED',
                    'token' => $token,
                    'user' => $user,
                    'role' => $user->groupap,
                    'minversion' => $MINIMUM_VERSION,
    			]);
    		}
    	}
    	return response()->json([
    		'status' => 'FAILED',
            'token' => 'Login Ditolak',
    	]);
    }

    private function validateInput($data) {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users|regex:/pertamina.com/',
            'password' => 'required|string|min:6',
		]);
	}
}
