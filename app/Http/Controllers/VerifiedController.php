<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use App\Booking;
use App\BookingLine;
use App\WfApproval;
use App\User;
use App\RfqLine;
use App\Rfq;

use App\Jobs\StartWorkflowApproval;

class VerifiedController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        // $this->middleware('auth');
    }

    public function approvalDetail($wfid)
    {
        $wf = WfApproval::find($wfid);

        // build generic object "name" => "value"
        $approvalobj = [];
        $positiveresponse = '';
        $negativeresponse = '';

        if($wf->ownertable == 'RFQ' && $wf->wfname == 'BIDDERLIST') {
            $rfq = Rfq::with(['rfqline'])->find($wf->ownerid);

            $attr = new \stdClass();
            $attr->name = 'Nomor Pengadaan';
            $attr->value = $rfq->rfqnum;
            array_push($approvalobj, $attr);

            $attr = new \stdClass();
            $attr->name = 'Judul Pengadaan';
            $attr->value = $rfq->title;
            array_push($approvalobj, $attr);

            $attr = new \stdClass();
            $attr->name = 'Metode Pengadaan';
            $attr->value = $rfq->procmethod;
            array_push($approvalobj, $attr);

            $attr = new \stdClass();
            $attr->name = 'Fungsi Pengguna';
            $attr->value = $rfq->fungsipengguna;
            array_push($approvalobj, $attr);

            $i = 1;
            foreach ($rfq->rfqline as $rfqline) {
                $attr = new \stdClass();
                $attr->name = 'Bidder ' . $i++;
                $attr->value = $rfqline->vendorname . ' (' . $rfqline->vendortype . ')';
                array_push($approvalobj, $attr);
            }

            $positiveresponse = 'APPROVE';
            $negativeresponse = 'REVISE';
        }

        if($wf->ownertable == 'BOOKING') {
            $booking = Booking::with(['passengers', 'requestor'])
                         ->find($wf->ownerid);
            
            $attr = new \stdClass();
            $attr->name = 'Jenis Request';
            $attr->value = $booking->bookingtype;
            array_push($approvalobj, $attr);

            $attr = new \stdClass();
            $attr->name = 'Status';
            $attr->value = $booking->wfstatus;
            array_push($approvalobj, $attr);

            $attr = new \stdClass();
            $attr->name = 'Requestor';
            $attr->value = $booking->requestor->name;
            array_push($approvalobj, $attr);

            $attr = new \stdClass();
            $attr->name = 'Asal';
            $attr->value = $booking->origin;
            array_push($approvalobj, $attr);

            $attr = new \stdClass();
            $attr->name = 'Tujuan';
            $attr->value = $booking->destination;
            array_push($approvalobj, $attr);

            $attr = new \stdClass();
            $attr->name = 'Berangkat';
            $attr->value = $booking->departuredate->format('d M Y H:i');
            array_push($approvalobj, $attr);

            $attr = new \stdClass();
            $attr->name = 'Sampai Dengan';
            $attr->value = $booking->returndate->format('d M Y H:i');
            array_push($approvalobj, $attr);

            if($booking->bookingtype == 'VOUCHER') {
                $attr = new \stdClass();
                $attr->name = 'Jumlah Voucher';
                $attr->value = $booking->quantity;
                array_push($approvalobj, $attr);
            }

            $positiveresponse = 'APPROVE';
            $negativeresponse = 'REJECT';
        }

        return response()->json([
            'status' => 'SUCCESS',
            'approval' => $approvalobj,
            'positiveresponse' => $positiveresponse,
            'negativeresponse' => $negativeresponse
        ]);
    }

    public function myapproval()
    {
        $user = Auth::user();

        $email = $user->email;
        $explodedEmail = explode('@', $email);
        $myapproval = WfApproval::where('wfassignee', $explodedEmail)
                        ->orderby('created_at', 'desc')
                        ->get();
        return response()->json([
            'status' => 'SUCCESS',
            'myapproval' => $myapproval
        ]);
    }

    public function myrequest()
    {
        $mybook = Booking::with(['passengers', 'passengers.userdepartment', 'car', 'driver'])
                    ->where('requestorid', Auth::id())
                    ->where('wfstatus', '<>', 'CANCEL')
                    ->orderby('created_at', 'desc')
                    ->get();

        $user = User::with(['department'])
                    ->where('id', Auth::id())
                    ->first();

        return response()->json([
            'status' => 'SUCCESS',
            'mybook' => $mybook,
            'me' => $user,
        ]);
    }

    public function cancelRequest($requestid)
    {
        $b = Booking::find($requestid);
        if($b->requestorid != Auth::id()) {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'Tidak Bisa Membatalkan Pesanan'
            ]);
        }
        if($b->wfstatus == 'ASSIGNED') {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'Tidak dapat membatalkan pesanan yang sudah diberikan kendaraan'
            ]);
        }

        $b->wfstatus = 'CANCEL';
        $b->save();

        // Wf Approval
        $wf = WfApproval::where('ownerid', $b->id)
                ->where('ownertable', 'BOOKING')
                ->first();
        $wf->status = 'CANCEL';
        $wf->wfassignee = '';
        $wf->save();

        return response()->json([
            'status' => 'SUCCESS',
            'message' => 'Berhasil membatalkan pesanan'
        ]);
    }

    public function saveNewBook(Request $request)
    {
        // di sini, arep lapo hehehe...
        $b = new Booking();
        $b->bookingtype = $request->bookingtype;
        $b->requestorid = Auth::user()->id;
        $b->origin = $request->origin;
        $b->destination = $request->destination;
        $b->departuredate = $request->departuredate;
        $b->returndate = $request->returndate;
        $b->quantity = $request->quantity;
        $b->wfstatus = 'SUBMIT';
        $b->remarks = $request->remarks;
        $b->save();

        // insert passengers
        foreach ($request->passengers as $p) {
            $bl = new BookingLine();
            $bl->bookingid = $b->id;
            $bl->name = $p['name'];
            $bl->department = $p['department']; // diisi department
            $bl->notes = $p['notes'];
            $bl->save();
        }

        // start workflow to CAMUNDA asynchronously
        // preparing payload
        $payload = new \stdClass();
        $payload->wfname = 'SEATREQUEST';
        $payload->processid = 'OneLevelApproval';
        $payload->ownertable = 'BOOKING';
        $payload->ownerid = $b->id;
        $payload->status = 'WAPPR';
        $payload->updatedby = Auth::user()->ldapname;

        // initiator
        $initiator = new \stdClass();
        $initiator->type = 'string';
        $initiator->value = Auth::user()->ldapname;

        $bpmVariables = new \stdClass();
        $bpmVariables->initiator = $initiator;

        // manager department nya saja
        $manager = new \stdClass();
        $manager->type = 'string';
        $userDep = User::with(['department'])->where('id', Auth::id())->first();
        $manager->value = 'maryulis'; //$userDep->department->managerldapname;
        $bpmVariables->firstapprover = $manager;

        // external api
        $extapi = new \stdClass();
        $extapi->type = 'string';
        $extapi->value =  'http://localhost/phiappb/api';
        $bpmVariables->EXTAPI = $extapi;

        $payload->variables = $bpmVariables;
        
        StartWorkflowApproval::dispatch($payload);

        return response()->json([
            'status' => 'SUCCESS',
            'booking' => $b
        ]);
    }
}
