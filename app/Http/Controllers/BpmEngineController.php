<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\WfApproval;
use App\WfHistory;
use App\Rfq;
use App\User;

use App\Events\NewAssignmentEvent;
use App\Mail\NewBookingMail;
use App\Jobs\StartWorkflowApproval;

class BpmEngineController extends Controller
{ 
    protected $BPM_ENGINE_URL = 'http://localhost:8000/engine-rest';
    protected $HTTP_HEADERS = ['Content-Type' => 'application/json', 'Accept' => 'application/json'];
    protected $PROCMGR = 'efriza.kamil';
    protected $PROCVP = 'akhmad.prayogi';

    // ============== BPM ENGINE RELATED ===================
    public function inboxAssignment($userid)
    {
        $user = User::find($userid);

        $email = $user->email;
        $explodedEmail = explode('@', $email);

        // request ke BPM
        $TASKURL = $this->BPM_ENGINE_URL  . '/task?assignee=' . $explodedEmail[0];
        $client = new Client();
        $r = $client->get($TASKURL, [
            'headers' => $this->HTTP_HEADERS
        ]);

        $body = json_decode($r->getBody(), true);
        $instanceids = [];
        foreach($body as $b) {
            $instanceids[] = $b['processInstanceId'];
        }

        // ini harus dicek dengan active wfapproval
        $wfapproval = WfApproval::whereIn('instanceid', $instanceids)
                        ->where('status', '!=', 'APPR')
                        ->get();

        return response()->json([
            'status' => 'SUCCESS',
            'wfapproval' => $wfapproval
        ]);
    }

    public function availableWorkflow($rfqid)
    {
        $workflows = DB::select("
            select vl.* from valuelist vl
            where vl.description not in (select wfname from wfapproval where ownerid = ?)
            and vl.category = 'WORKFLOWS'
            order by vl.name
        ", [$rfqid]);

        return response()->json([
            'status' => 'SUCCESS',
            'workflows' => $workflows
        ]);
    }

    public function workflowHistory($wfid)
    {
        $wfhistory = WfHistory::where('wfapprovalid', $wfid)
                        ->select(DB::raw("id, wfapprovalid, status, memo, (select name from users where users.email = concat(wfhistory.updatedby, '@pertamina.com')) as updatedby, DATE_FORMAT(created_at, '%d %b %Y %H:%i') created "))
                        ->orderby('created_at', 'asc')
                        ->get();
        return response()->json([
            'status' => 'SUCCESS',
            'wfhistory' => $wfhistory
        ]);
    }

    public function workflowStatus($rfqid)
    {
        $workflowstatus = WfApproval::where('ownerid', $rfqid)
                            ->where('ownertable', 'RFQ')
                            ->select(DB::raw("id, wfname, status, instanceid, ownerid, wfassignee, (select name from users where SUBSTRING_INDEX(email, '@', 1) = TRIM(wfassignee)) as assignee, updated_at "))
                            ->orderby('updated_at', 'asc')
                            ->get();
        return response()->json([
            'status' => 'SUCCESS',
            'wfstatus' => $workflowstatus,
        ]);
    }

    public function completeAssignment(Request $request)
    {
	    //$rfq = Rfq::find($request->rfqid);
	    //
	//Log::info(print_r($request->all(), true));
        $wfinstance = WfApproval::find($request->id);

        $WFURL = $this->BPM_ENGINE_URL  . '/task?processInstanceId=' . $wfinstance->instanceid
                    . '&assignee=' . strtolower($request->currentuser);
        $client = new Client();

        try {
            $r = $client->get($WFURL, ['headers' => $this->HTTP_HEADERS]);
            $body = json_decode($r->getBody(), true);
        }
        catch(RequestException $e) {
		Log::error($e);
        }
        

        if(isset($body[0]['id'])) {
            // submit form
            $WFURL = $this->BPM_ENGINE_URL  . '/task/' . $body[0]['id'] . '/submit-form';
          
            // call engine
            $client = new Client();

            try {
                $resp = $client->post($WFURL, [
                    'headers' => $this->HTTP_HEADERS,
                    'body' => '{
                        "variables": {
                            "approvalstatus": {
                                "value": "' . $request->approvalstatus . '",
                                "type": "string"
                            },
                            "memo": {
                                "value": "' . $request->memo . '",
                                "type": "string"
                            }
                        }
                    }'
                ]);
            }
            catch(\GuzzleHttp\Exception\RequestException $e) {
                return response()->json([
                    'status' => 'ERROR',
                    'message' => "Failed to start workflow"
                ]); 
            }
            

            // update wfapproval
            $WFURL = $this->BPM_ENGINE_URL  . '/task?processInstanceId=' . $wfinstance->instanceid;
            $client = new Client();
            $r = $client->get($WFURL, ['headers' => $this->HTTP_HEADERS]);
            $body = json_decode($r->getBody(), true);

            $assignee = '';
            foreach($body as $b) {
                $assignee .= ' ' . $b['assignee'];
            }
            $wfinstance->wfassignee = $assignee;
            $wfinstance->save();

            if($assignee != '') {
                event(new NewAssignmentEvent($assignee));
            }

            // reload wfapproval
            // reload workflow status
            $workflowstatus = WfApproval::where('ownerid', $wfinstance->ownerid)
                ->where('ownertable', $wfinstance->ownertable)
                ->select(DB::raw("id, wfname, status, instanceid, ownerid, wfassignee, (select name from users where SUBSTRING_INDEX(email, '@', 1) = TRIM(wfassignee)) as assignee, updated_at "))
                ->orderby('updated_at', 'asc')
                ->get();
            
            $wfapproval = WfApproval::where('ownerid', $wfinstance->ownerid)
                ->where('ownertable', $wfinstance->ownertable)
                ->select(DB::raw("id, wfname, status, instanceid, ownerid, wfassignee, (select name from users where SUBSTRING_INDEX(email, '@', 1) = TRIM(wfassignee)) as assignee, updated_at "))
                ->whereNotIn('status', ['APPR'])
                ->first();

            return response()->json([
                'status' => 'SUCCESS',
                'wfstatus' => $workflowstatus,
                'wfapproval' => $wfapproval
            ]);
        }
        else {
            return response()->json([
                'status' => 'ERROR',
                'message' => "You don't have assignment on this workflow"
            ]);
        }

        return response()->json([
            'status' => 'ERROR',
            'message' => 'Error completing workflow'
        ]);
    }

    public function startWorkflow(Request $request)
    {
        $rfq = Rfq::find($request->rfqid);
        // yang boleh start hanya buyernya

        $payload = new \stdClass();
        $payload->wfname = $request->wfname;
        $payload->ownertable = 'RFQ';
        $payload->ownerid = $rfq->id;
        $payload->status = 'WAPPR';
        $payload->updatedby = Auth::user()->ldapname;

        // initiator
        $initiator = new \stdClass();
        $initiator->type = 'string';
        $initiator->value = Auth::user()->ldapname;

        $bpmVariables = new \stdClass();
        $bpmVariables->initiator = $initiator;

        // external api
        $extapi = new \stdClass();
        $extapi->type = 'string';
        $extapi->value =  'http://localhost/phiappb/api';
        $bpmVariables->EXTAPI = $extapi;

        switch ($request->wfname) {
            case 'BIDDERLIST':
                $payload->processid = 'bidder-list';

                $procmgrObj = new \stdClass();
                $procmgrObj->type = 'string';
                $procmgrObj->value = $this->PROCMGR;
                $bpmVariables->procmgr = $procmgrObj;

                $procvpObj = new \stdClass();
                $procvpObj->type = 'string';
                $procvpObj->value = $this->PROCVP;
                $bpmVariables->procvp = $procvpObj;

                break;
            default:
                break;
        }

        $payload->variables = $bpmVariables;
        StartWorkflowApproval::dispatch($payload);
        
        // // start camunda workflow 
        // if($request->wfname == 'BIDDERLIST') {
        //     $WFURL = $this->BPM_ENGINE_URL . '/process-definition/key/bidder-list/start';
        //     // call engine
        //     $client = new Client();
        //     try {
        //         $resp = $client->post($WFURL, [
        //             'headers' => $this->HTTP_HEADERS,
        //             'body' => '{
        //                 "variables": {
        //                     "procmgr": {
        //                         "value": "' . $this->PROCMGR . '",
        //                         "type": "string"
        //                     },
        //                     "procvp": {
        //                         "value": "' . $this->PROCVP . '",
        //                         "type": "string"
        //                     },
        //                     "initiator": {
        //                         "value": "' . $rfq->buyer .'",
        //                         "type": "string"
        //                     }, 
        //                     "EXTAPI": {
        //                         "value": "http://localhost/phiappb/api",
        //                         "type": "string"
        //                     }
        //                 }
        //             }'
        //         ]);
    
        //         $body = json_decode($resp->getBody(), true);
        //         $instanceid = $body['id'];
    
        //         // dapatkan current assignment dari wf ini
        //         $TASKURL = $this->BPM_ENGINE_URL  . '/task?processInstanceId=' . $instanceid;
        //         $r = $client->get($TASKURL, [
        //             'headers' => $this->HTTP_HEADERS
        //         ]);
        //         $body = json_decode($r->getBody(), true);
        //         $assignee = $body[0]['assignee'];

        //         // register event
        //         event(new NewAssignmentEvent($assignee));
    
        //         // simpan di tabel
        //         $wf = WfApproval::create([
        //             'wfname' => 'BIDDERLIST',
        //             'status' => 'WAPPR',
        //             'instanceid' => $instanceid,
        //             'ownerid' => $request->rfqid,
        //             'ownertable' => 'RFQ',
        //             'wfassignee' => $assignee
        //         ]);
    
        //         // history
        //         WfHistory::create([
        //             'wfapprovalid' => $wf->id,
        //             'status' => 'WAPPR',
        //             'memo' => 'Workflow started',
        //             'updatedby' => Auth::user()->ldapname
        //         ]);
    
        //         // update rfq-nya
        //         $rfq->wftype = 'BIDDERLIST';
        //         $rfq->wfstatus = 'WAPPR';
        //         $rfq->save();
        //     }
        //     catch(RequestException $e) {
        //         Log::error($e);
        //         return response()->json([
        //             'status' => 'ERROR',
        //             'response' => 'Failed to start workflow'
        //         ]);
        //     }
        // }

        // reload workflow status
        $workflowstatus = WfApproval::where('ownerid', $request->rfqid)
                            ->where('ownertable', 'RFQ')
                            ->select(DB::raw("id, wfname, status, instanceid, ownerid, wfassignee, (select name from users where SUBSTRING_INDEX(email, '@', 1) = TRIM(wfassignee)) as assignee "))
                            ->orderby('updated_at', 'asc')
                            ->get();
        
        $wfapproval = WfApproval::where('ownerid', $request->rfqid)
            ->where('ownertable', 'RFQ')
            ->select(DB::raw("id, wfname, status, instanceid, ownerid, wfassignee, (select name from users where SUBSTRING_INDEX(email, '@', 1) = TRIM(wfassignee)) as assignee "))
            ->whereNotIn('status', ['APPR'])
            ->first();                            

        return response()->json([
            'status' => 'SUCCESS',
            // 'response' => $body,
            'wfstatus' => $workflowstatus,
            'wfapproval' => $wfapproval
        ]);
    }

    public function changeStatus(Request $request, $status, $completedby, $processinstanceid)
    {
        Log::info('Change status invoked');
        $wf = WfApproval::where('instanceid', $processinstanceid)->first();
        $wf->status = $status;
        $wf->save();

        $decodedMemo = base64_decode($request->input('memo'));

        // update 
        
        DB::statement("
            update " . strtolower($wf->ownertable) . " set wfstatus = '$status' where id = '$wf->ownerid'
        ");
        
        // event change status
        if($wf->ownertable == 'BOOKING' && $status == 'APPR') {
            // kirim notifikasi ke transport dispatcher
            Mail::to('dt.phi@pertamina.com')
                ->bcc('galih.satriaji@pertamina.com')
                ->queue(new NewBookingMail());
        }

        // history
        WfHistory::create([
            'wfapprovalid' => $wf->id,
            'status' => $status,
            'memo' => $decodedMemo,
            'updatedby' => $completedby
        ]);

        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }

    public function sendEmail($taskid)
    {
        Log::info('sendEmail API invoked...' . $taskid);

        // ini yang harus masuk queue dan update current assignment WFAPPROVAL
        // $client = new Client();

        // // dapatkan current assignment dari wf ini
        // $TASKURL = env('BPM_ENGINE_URL') . '/task/' . $taskid;
        // $r = $client->get($TASKURL, [
        //     'headers' => $this->HTTP_HEADERS
        // ]);
        // $body = json_decode($r->getBody(), true);
        // $assignee = $body['assignee'];

        // send email
        

        return 'SUCCESS';
    }
}
