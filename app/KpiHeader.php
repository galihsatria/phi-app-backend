<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KpiHeader extends Model
{
    protected $table = 'kpiheader';

    protected $fillable = ['apgroup', 'period', 'achievement', 'organization', 'highlight', 'nextactionplan', 'events'];
}
