<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WfHistory extends Model
{
    protected $table = 'wfhistory';

    protected $fillable = [
        'wfapprovalid', 'status', 'memo', 'updatedby'
    ];

    protected $dates = ['created_at'];

}
