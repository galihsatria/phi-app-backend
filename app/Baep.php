<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Baep extends Model
{
    protected $table = 'baep';

    protected $fillable = ['rfqid', 'section', 'content', 'order'];
}
