<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    protected $table = 'phidocuments';

    protected $fillable = [
        'owner', 'ownerid', 'category', 'docname'
    ];
}
