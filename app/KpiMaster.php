<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KpiMaster extends Model
{
    protected $table = 'kpimaster';
}
