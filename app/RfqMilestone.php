<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RfqMilestone extends Model
{
    protected $table = 'rfqmilestone';

    protected $fillable = ['rfqid', 'aktivitas', 'startdate', 'enddate'];

    protected $dates = ['startdate', 'enddate'];
}
