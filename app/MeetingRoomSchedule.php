<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingRoomSchedule extends Model
{
    protected $table = 'meetingroom_schedule';

    protected $fillable = ['roomid', 'name', 'organizer',
    'meetingdate', 'starttime', 'finishtime', 'quantity'];

    protected $dates = ['meetingdate'];

    public function room()
    {
        return $this->belongsTo('App\MeetingRoom', 'roomid', 'id');
    }

    public function getMeetingdateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }
}
