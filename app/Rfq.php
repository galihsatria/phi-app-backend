<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rfq extends Model
{
    protected $table = 'rfq';

    protected $fillable = [
        'rfqnum', 'title', 'rfqtype', 'proctype',
        'procmethod', 'prnum', 'documentdate',
        'prdate', 'buyer', 'buyerid', 'procmgr', 'procvp',
        'wftype', 'wfstatus', 'createdby', 'bidopeningdate', 'bidopeningdate2',
        'prebiddate', 'invitetobidletternum', 'bidopeningletternum', 
        'memoadminevalletternum', 'technicalevalletternum', 'evalresultletternum', 'currencycode',
        'ownerestimate', 'sistemevaluasipenawaran', 'periode',
        'justifikasimetode', 'sampul', 'awardletternum', 'invitationdate', 'ambildokumendate',
        'tkdn', 'fungsipengguna', 'estminvalue', 'estmaxvalue'
    ];

    protected $dates = ['prdate', 'documentdate', 'bidopeningdate', 'bidopeningdate2', 'prebiddate', 'invitationdate', 'ambildokumendate'];

    public function rfqline()
    {
        return $this->hasMany('App\RfqLine', 'rfqid', 'id');
    }

    public function wfapproval()
    {
        return $this->hasMany('App\WfApproval', 'rfqid', 'id');
    }

    public function buyer()
    {
        return $this->belongsTo('App\User', 'buyerid', 'id');
    }

    public function doclist()
    {
        return $this->hasMany('App\Documents', 'ownerid', 'id')
                    ->where('owner', 'RFQ');
    }

    // ====

    public function getDocumentdateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function getPrdateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function getBidopeningdateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function getBidopeningdate2Attribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function getPrebiddateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function getInvitationdateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function getAmbildokumendateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }
}
