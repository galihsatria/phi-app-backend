<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnicalEval extends Model
{
    protected $table = 'technicaleval';

    protected $fillable = [
        'rfqid', 'vendorid', 'hasil', 'keterangan',
        'hasilkomersial', 'keterangankomersial', 'winner',
        'quoteprice', 'afternegoprice'
    ];
}
