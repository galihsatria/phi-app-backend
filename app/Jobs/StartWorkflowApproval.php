<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use GuzzleHttp\Client;
use App\WfApproval;
use App\WfHistory;

use App\Events\NewAssignmentEvent;

class StartWorkflowApproval implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $payload;
    protected $BPM_ENGINE_URL = 'http://localhost:8000/engine-rest';
    protected $HTTP_HEADERS = ['Content-Type' => 'application/json', 'Accept' => 'application/json'];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payload)
    {
        $this->payload = $payload;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Log::info(json_encode($this->payload->updatedby));
        $WFURL = $this->BPM_ENGINE_URL . '/process-definition/key/' . $this->payload->processid . '/start'; 
            
        // call engine
        $client = new Client();
        Log::info(json_encode($this->payload->variables));
        $resp = $client->post($WFURL, [
            'headers' => $this->HTTP_HEADERS,
            'body' => '{
                "variables":' . json_encode($this->payload->variables) . '
            }' 
        ]);

        $body = json_decode($resp->getBody(), true);
        $instanceid = $body['id'];

        // dapatkan current assignment dari wf ini
        $TASKURL = $this->BPM_ENGINE_URL . '/task?processInstanceId=' . $instanceid;
        $r = $client->get($TASKURL, [
            'headers' => $this->HTTP_HEADERS
        ]);
        $body = json_decode($r->getBody(), true);
        $assignee = $body[0]['assignee'];

        // register event
        event(new NewAssignmentEvent($assignee));

        // simpan di tabel
        $wf = WfApproval::create([
            'wfname' => $this->payload->wfname,
            'status' => $this->payload->status,
            'instanceid' => $instanceid,
            'ownertable' => $this->payload->ownertable,
            'ownerid' => $this->payload->ownerid,
            'wfassignee' => $assignee
        ]);

        // history
        WfHistory::create([
            'wfapprovalid' => $wf->id,
            'status' => $this->payload->status,
            'memo' => 'Workflow started',
            'updatedby' => $this->payload->updatedby
        ]);

        // update ownertable-nya
        DB::statement("
            update " . strtolower($this->payload->ownertable) . "
            set wftype = '" . $this->payload->wfname . "', wfstatus = '" . $this->payload->status . "'
            where id = '" . $this->payload->ownerid . "'
        ");
        // $rfq->wftype = 'BIDDERLIST';
        // $rfq->wfstatus = 'WAPPR';
        // $rfq->save();
    }
}
