<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model
{
    protected $table = 'userdevices';

    protected $fillable = ['userid', 'deviceid', 'subscribed'];
}
