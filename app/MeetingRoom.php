<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingRoom extends Model
{
    protected $table = 'meetingroom';

    protected $fillable = ['name', 'floor', 'capacity'];
}
