<?php

namespace App\Listeners;

use App\Events\NewAssignmentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use GuzzleHttp\Client;

class AssignmentPushNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewAssignmentEvent  $event
     * @return void
     */
    public function handle(NewAssignmentEvent $event)
    {
        $assignee = $event->assignee;
        // cari user devices
        $user = User::with(['devices'])
                    ->where('email', 'like', $assignee . '%')
                    // ->where('subscribed', '1')
                    ->first();
        $ONESIGNALURL = 'https://onesignal.com/api/v1/notifications';
        foreach ($user->devices as $device) {
            // kirim push notifikasi ke sini
            $playerids = array($device->deviceid);
            $client = new Client();
            try {
                $resp = $client->post($ONESIGNALURL, [
                    'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                    'body' => '{
                        "app_id": "7f2d9523-b996-4484-a9e8-ec75de4d2470",
                        "headings": {"en": "New Workflow Assignment!"},
                        "contents": {"en": "Ada assignment baru yang perlu segera di-follow-up"},
                        "include_player_ids": ' . json_encode($playerids) . '
                    }'
                ]);
                // Log::info($resp->getBody());
            } catch (\Throwable $th) {
                // Log::error($th);
                continue;
            }
        }
    }
}
