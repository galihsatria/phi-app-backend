<?php

namespace App\Listeners;

use App\Events\NewAssignmentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\Mail\AssignmentMail;

class NewAssignmentEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewAssignmentEvent  $event
     * @return void
     */
    public function handle(NewAssignmentEvent $event)
    {
        $assignee = $event->assignee;
        Log::info('Event tertangkap kamera: ' . $assignee);

        $user = User::where('email', 'like', $assignee . '%')
                    ->first();
        if($user != null) {
            // kirim email
            Mail::to($user->email)->queue(new AssignmentMail($user));
        }
    }
}
