<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorCommodities extends Model
{
    protected $table = 'vendor_commodities';

    protected $fillable = [
        'clientid', 'vendorid', 'commid', 'description', 'localupdated'
    ];
}
