<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name', 'parentid', 'manageremail'];
    
    public function getManagerldapnameAttribute() {
        $email = $this->manageremail;
        $explodedEmail = explode('@', $email);
        return trim($explodedEmail[0]);
    }
}
