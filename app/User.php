<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'departmentid', 'groupap', 'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getLdapnameAttribute()
    {
        $email = $this->email;
        $explodedEmail = explode('@', $email);
        return trim($explodedEmail[0]);
    }

    public function department()
    {
        return $this->belongsTo('App\Department', 'departmentid', 'id');
    }

    public function authorizations()
    {
        return $this->hasMany('App\UserAuth', 'userid', 'id');
    }
    
    public function devices()
    {
        return $this->hasMany('App\UserDevice', 'userid', 'id');
    }

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }
}
