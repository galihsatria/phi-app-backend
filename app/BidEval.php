<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BidEval extends Model
{
    protected $table = 'bideval';

    protected $fillable = [
        'rfqid', 'vendorid', 'evaladminno', 'evaladminresult'
    ];

    public function vendor()
    {
        return $this->belongsTo('App\Vendor', 'vendorid', 'vendorid');
    }
}
