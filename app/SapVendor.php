<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SapVendor extends Model
{
    protected $table = 'vendor';

    protected $fillable = [
        'clientid', 'vendorid', 'type', 'csms', 'name', 'name2',
        'city', 'district', 'street', 'deleteflag', 'postingblock',
        'purchaseblock', 'taxnumber', 'telephone1', 'telephone2',
        'faxnumber', 'email', 'sales', 'salesphone', 
    ];

    public function commodities()
    {
        return $this->hasMany('App\VendorCommodities', 'vendorid', 'vendorid');
    }

    public function letters()
    {
        return $this->hasMany('App\VendorLetter', 'vendorid', 'vendorid');
    }
}
