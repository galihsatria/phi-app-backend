<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WfApproval extends Model
{
    protected $table = 'wfapproval';

    protected $fillable = ['wfname', 'status', 'instanceid', 'ownerid', 'wfassignee', 'payload', 'ownertable' ];

    public function getUpdatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d M Y H:i');
    }

    public function setWfassigneeAttribute($value)
    {
        $this->attributes['wfassignee'] = trim($value);
    }
}
