<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingLine extends Model
{
    protected $table = 'bookingline';

    protected $fillable = [
        'bookingid', 'name', 'description', 'department'
    ];

    public function userdepartment()
    {
        return $this->belongsTo('App\Department', 'department', 'id');
    }
}
