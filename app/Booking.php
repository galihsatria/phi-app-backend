<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 'booking';

    protected $fillable = [
        'bookingtype', 'requestorid', 'origin', 'destination',
        'departuredate', 'returndate', 'quantity', 'wftype', 'wfstatus',
        'remarks', 'transportremarks', 'assignedcarid', 'assigneddriverid',
    ];

    protected $dates = ['departuredate', 'returndate'];

    public function passengers()
    {
        return $this->hasMany('\App\BookingLine', 'bookingid', 'id');
    }

    public function requestor()
    {
        return $this->belongsTo('\App\User', 'requestorid', 'id');
    }

    public function car()
    {
        return $this->belongsTo('\App\Vehicle', 'assignedcarid', 'id');
    }

    public function driver()
    {
        return $this->belongsTo('\App\Driver', 'assigneddriverid', 'id');
    }
}
