<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $table = 'drivers';

    protected $fillable = ['id', 'email', 'phone', 'name'];

    public function user()
    {
        return $this->belongsTo('App\User', 'email', 'email');
    }
}
