<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BidControl extends Model
{
    protected $table = 'bidcontrol';

    protected $fillable = [
        'rfqnum', 'title', 'department', 'openingdate', 'closingdate', 'submitdate',
        'bidopeningdate', 'meetingdate', 'vendorinvited', 'vendorrejected', 'vendorsubmitted',
        'vendorsubmittedontime', 'vendorsubmittedlate', 'vendornotsubmitted', 'routestatus',
        'buyer',
    ];
}
