<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorLetter extends Model
{
    protected $table = 'vendor_letter';

    protected $dates = ['sktvaliditydate'];

    protected $fillable = [
        'vendorid', 'lettertype', 'description', 'sktvaliditydate', 'aktapendirianno'
    ];

    public function getSktvaliditydateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }
}
