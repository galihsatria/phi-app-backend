<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RfqLine extends Model
{
    protected $table = 'rfqline';
    protected $fillable = [
        'rfqid', 'vendorname', 'vendortype',
        'sktnum', 'sanksi', 'exp_contracttitle',
        'exp_companies', 'remarks', 'vendorid',
        'ambildokumen', 'kelengkapandokumen', 'golusaha'
    ];
}
