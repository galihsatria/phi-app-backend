<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'phivendor';

    protected $fillable = [
        'ownerid', 'name', 'civdid', 'sapid', 'clearpasswd', 'useremail',
        'verified', 'verifieddate'
    ];

    protected $dates = ['verifieddate'];

    public function sapvendor()
    {
        return $this->belongsTo('App\SapVendor', 'sapid', 'vendorid');
    }

    public function getVerifieddateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    
}
