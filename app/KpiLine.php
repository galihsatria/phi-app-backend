<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KpiLine extends Model
{
    protected $table = 'kpiline';

    protected $fillable = ['kpiheaderid', 'kpiid', 'value'];
}
