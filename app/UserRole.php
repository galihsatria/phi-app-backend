<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'user_roles';

    protected $fillable = [
        'role', 'userid', 'name', 'level'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'userid', 'id');
    }
}
