@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Verified!</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Terima kasih, email Anda sudah terverifikasi dan bisa digunakan untuk login di aplikasi.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
