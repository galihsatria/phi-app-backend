<!DOCTYPE html>
<html>
<head>
    <title>Pesanan Anda Telah Diberikan Kendaaran</title>
</head>
 
<body style="font-family: Arial, Helvetica, sans-serif">
<h2>DH Bapak/Ibu Pemesan,</h2>
<br/>
Pesanan Anda telah di-follow up oleh Transport Dispatcher. Berikut ini adalah detail pesanan Anda:
<table>
    <tr>
        <td>Asal:</td>
        <td>{{ $booking->origin }}</td>
    </tr>
    <tr>
        <td>Tujuan:</td>
        <td>{{ $booking->destination }}</td>
    </tr>
    <tr>
        <td>Berangkat:</td>
        <td>{{ $booking->departuredate->format('d M Y H:i') }}</td>
    </tr>
    <tr>
        <td>Sampai Dengan:</td>
        <td>{{ $booking->returndate->format('d M Y H:i') }}</td>
    </tr>
    <tr>
        <td>Kendaraan:</td>
        <td>{{ $booking->car->vehicletype }} - {{ $booking->car->nomorpolisi }}</td>
    </tr>
    <tr>
        <td>Driver:</td>
        <td>{{ $booking->driver->name }} - {{ $booking->driver->phone }}</td>
    </tr>
    <tr>
        <td>Catatan Tambahan:</td>
        <td>{{ $booking->transportremarks }}</td>
    </tr>
</table>
<br/>

Demikian disampaikan terima kasih.
</body>
 
</html>