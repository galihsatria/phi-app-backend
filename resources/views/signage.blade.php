<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="refresh" content="900">
    <!-- <link rel="icon" href="../../../../favicon.ico"> -->

    <title>PHI Signage</title>

    <!-- Bootstrap core CSS -->
    <link href="signage/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="signage/carousel.css" rel="stylesheet">

    <link href="https://vjs.zencdn.net/7.4.1/video-js.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
  </head>
  <body>
      <div class="container-fluid">
          <div class="row">
              <div class="col-sm-5 leftcol">
                <div class="row">
                  <div class="col-sm lefttop">
                      <iframe width="100%" height="100%" src="https://www.youtube.com/embed/cY3EJDH1VUM" frameborder="0" 
                      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
                </div>
                <div class="row">
                    <div class="col-sm leftbottom">
                        <img src="http://10.254.200.130/phiappb/ramadhan.jpg"  />
                    </div>
                </div>
              </div>
              <div class="col-sm rightcol">
                    <div class="row">
                        <div class="col" style="text-align:right"><h1 class="h3" style="margin-right: 20px; margin-top: 20px;">{{ $today }}</h1></div>
                    </div>
                    
                    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="10000">
                            <ol class="carousel-indicators">
                              @for($i = 0; $i < $pages; $i++)
                                <li data-target="#myCarousel" data-slide-to="{{ $i }}" class="{{ $i == 0 ? 'active' : ''}}"></li>
                              @endfor
                            </ol>
                            <div class="carousel-inner">
                                @for($i = 0; $i < $pages; $i++)
                              <div class="carousel-item {{ $i == 0 ? 'active' : ''}}">
                                    <div class="carousel-caption text-left">
                                        <div style="display: block; height: 20px"></div>
                                            <div class="list-group">
                                                @for($y = $i * $itemsperpage; $y < ($i * $itemsperpage + $itemsperpage); $y++)
                                                @if($y >= count($schedules))
                                                    <?php break; ?>
                                                @endif
                                                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                                    <div class="d-flex w-100 justify-content-between">
                                                    <div style="font-size: 26px; color: black">{{ $schedules[$y]->name }}</div>
                                                    <p style="font-size: 20px" class="text-muted">{{ $schedules[$y]->starttime }} 
                                                        - {{ $schedules[$y]->finishtime }}</p>
                                                    </div>
                                                    <p style="font-size: 21px" class="mb-1">
                                                            {{ $schedules[$y]->room->name }} Lantai {{ $schedules[$y]->room->floor }}.
                                                            <br>
                                                            {{ $schedules[$y]->organizer }}
                                                    </p>
                                                </a>
                                                @endfor
                                            </div>
                                                  

                                    </div>
                              </div>
                              @endfor
                                  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                            </div>
                            
                            
                          </div>
              </div>
          </div>
      </div>

    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="signage/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="signage/js/vendor/popper.min.js"></script>
    <script src="signage/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="signage/js/vendor/holder.min.js"></script>
  </body>
</html>
