@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-responsive">
                <thead>
                    <th>Nama</th>
                    <th>Alamat Lengkap</th>
                    <th>Nama Kontak</th>
                    <th>Nomor Telepon</th>
                    <th>Email 1</th>
                    <th>Email 2</th>
                </thead>
                <tbody>
                    @foreach($vendor as $v)
                    <tr>
                        <td>{{ $v->namaentitas }}</td>
                        <td>{{ $v->alamatlengkap }}</td>
                        <td>{{ $v->namakontak }}</td>
                        <td>{{ $v->nomortelepon }}</td>
                        <td>{{ $v->email1 }}</td>
                        <td>{{ $v->email2 }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection