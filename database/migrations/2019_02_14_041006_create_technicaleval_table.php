<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechnicalevalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technicaleval', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rfqid')->index();
            $table->string('vendorid', 16)->index();
            $table->string('hasil', 16)->default('TIDAK LULUS');
            $table->string('keterangan')->nullable();
            // commercial eval
            $table->string('hasilkomersial', 16)->nullable();
            $table->string('keterangankomersial')->nullable();
            $table->boolean('winner')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technicaleval');
    }
}
