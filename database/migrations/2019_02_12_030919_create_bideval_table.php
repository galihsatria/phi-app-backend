<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidevalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bideval', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rfqid')->index();
            $table->string('vendorid', 16)->index();
            $table->string('evaladminno', 8);
            $table->string('evaladminresult', 8)->default('N/A');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bideval');
    }
}
