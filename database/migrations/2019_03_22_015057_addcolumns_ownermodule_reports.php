<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddcolumnsOwnermoduleReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function($table) {
            $table->string('ownermodule', 16)->nullable();
            $table->string('param1', 16)->nullable();
            $table->string('param2', 16)->nullable();
            $table->string('param3', 16)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function($table) {
            $table->dropColumn(['ownermodule', 'param1', 'param2', 'param3']);
        });
    }
}
