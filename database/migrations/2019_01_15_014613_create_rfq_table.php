<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfq', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rfqnum', 16)->index();
            $table->string('title', 400);
            $table->string('rfqtype', 8); // BARANG atau JASA
            $table->string('proctype', 64);
            $table->string('procmethod', 64);
            $table->string('prnum', 16)->index();
            $table->date('documentdate');
            $table->date('prdate');
            $table->string('buyer');
            $table->string('procmgr');
            $table->string('procvp');
            $table->string('wftype')->nullable();
            $table->string('wfstatus')->nullable();
            $table->unsignedInteger('createdby');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rfq');
    }
}
