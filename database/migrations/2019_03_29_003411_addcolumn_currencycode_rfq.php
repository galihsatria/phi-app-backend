<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddcolumnCurrencycodeRfq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rfq', function($table) {
            $table->string('currencycode', 3)->default('IDR');
            $table->date('bidopeningdate2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rfq', function($table) {
            $table->dropColumn(['currencycode', 'bidopeningdate2']);
        });
    }
}
