<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddcolumnVendorBankusd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor', function($table) {
            $table->string('bankname2')->nullable();
            $table->string('bankaccountno2')->nullable();
            $table->string('golusaha', 8)->nullable();
            $table->float('csmsscore')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor', function($table) {
            $table->dropColumn(['bankname2', 'bankaccountno2', 'golusaha', 'csmsscore']);
        });
    }
}
