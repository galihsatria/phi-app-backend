<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddcolumnsVerifiedPhivendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phivendor', function($table) {
            $table->boolean('verified')->default(false);
            $table->date('verifieddate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phivendor', function($table) {
            $table->dropColumn(['verified', 'verifieddate']);
        });
    }
}
