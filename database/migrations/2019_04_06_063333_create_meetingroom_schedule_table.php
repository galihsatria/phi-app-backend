<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingroomScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetingroom_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('roomid')->index();
            $table->string('name');
            $table->string('organizer');
            $table->date('meetingdate');
            $table->string('starttime');
            $table->string('finishtime');
            $table->smallInteger('quantity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetingroom_schedule');
    }
}
