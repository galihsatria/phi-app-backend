<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpiheaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpiheader', function (Blueprint $table) {
            $table->increments('id');
            $table->string('apgroup', 16);
            $table->string('period', 8);
            $table->string('achievement', 2000)->nullable();
            $table->string('events', 2000)->nullable();
            $table->string('organization', 2000)->nullable();
            $table->string('highlight', 2000)->nullable();
            $table->string('nextactionplan', 2000)->nullable();
            $table->string('wfstatus', 16)->default('DRAFT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpiheader');
    }
}
