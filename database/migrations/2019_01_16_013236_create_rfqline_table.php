<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfqlineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfqline', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rfqid');
            $table->string('vendorname');
            $table->string('vendortype', 8);
            $table->string('sktnum', 16);
            $table->string('sanksi', 16);
            $table->string('exp_contracttitle', 500)->nullable();
            $table->string('exp_companies', 500)->nullable();
            $table->string('remarks', 2000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rfqline');
    }
}
