<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('vendor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clientid');
            $table->string('vendorid', 16)->unique();
            $table->string('type', 8)->nullable();
            $table->string('csms', 1)->nullable();
            $table->string('name', 2000);
            $table->string('name2', 2000)->nullable();
            $table->string('city', 1000)->nullable();
            $table->string('district', 1000)->nullable();
            $table->string('street', 1000)->nullable();
            $table->boolean('deleteflag')->default(false);
            $table->boolean('postingblock')->default(false);
            $table->boolean('purchaseblock')->default(false);
            $table->string('taxnumber', 200)->nullable();
            $table->string('telephone1', 200)->nullable();
            $table->string('telephone2', 200)->nullable();
            $table->string('faxnumber', 200)->nullable();
            $table->string('email', 1000)->nullable();
            $table->string('sales', 200)->nullable();
            $table->string('salesphone', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor');
    }
}
