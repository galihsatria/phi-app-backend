<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddcolumnAmbildokumenRfqline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rfqline', function($table) {
            $table->boolean('ambildokumen')->default(false);
            $table->boolean('kelengkapandokumen')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rfqline', function($table) {
            $table->dropColumn(['ambildokumen', 'kelengkapandokumen']);
        });
    }
}
