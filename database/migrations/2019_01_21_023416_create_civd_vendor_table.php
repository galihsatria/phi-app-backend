<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCivdVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('civd_vendor', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('vendorid')->nullable();
            $table->unsignedInteger('processid')->nullable();
            $table->unsignedInteger('registrationid');
            $table->string('tipeentitas');
            $table->string('namaentitas');
            $table->string('statusentitas')->nullable();
            $table->string('statusaktivitas')->nullable();
            $table->unsignedInteger('durasi')->default(0);
            $table->string('tanggalinisiasi')->nullable();
            $table->string('alamatlengkap', 2000)->nullable();
            $table->string('kodepos')->nullable();
            $table->string('kkks')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota')->nullable();
            $table->string('nomortelepon')->nullable();
            $table->string('nomorfax')->nullable();
            $table->string('namakontak')->nullable();
            $table->string('email1')->nullable();
            $table->string('email2')->nullable();
            $table->string('npwp')->nullable();
            $table->string('suketdomisili')->nullable();
            $table->string('mulaiberlaku')->nullable();
            $table->string('akhirberlaku')->nullable();
            $table->string('idspda')->nullable();
            $table->string('nomorspda')->nullable();
            $table->string('mulaiberlakuspda')->nullable();
            $table->string('akhirberlakuspda')->nullable();
            $table->string('namafile')->nullable();
            $table->string('diterbitkanoleh')->nullable();
            $table->string('metodeupload')->nullable();
            $table->string('sanksi')->nullable();
            $table->string('pemberisanksi')->nullable();
            $table->string('partisipan')->nullable();
            $table->string('tanggalkomplit')->nullable();
            $table->string('catatanskk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('civd_vendor');
    }
}
