<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddcolumnLetternumRfq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rfq', function($table) {
            $table->string('sistemevaluasipenawaran')->nullable();
            $table->integer('sampul')->nullable();
            $table->string('justifikasimetode', 2000)->nullable();
            $table->double('ownerestimate')->nullable();
            $table->integer('periode')->nullable();
            $table->string('invitetobidletternum')->nullable();
            $table->string('bidopeningletternum')->nullable();
            $table->string('memoadminevalletternum')->nullable();
            $table->string('technicalevalletternum')->nullable();
            $table->string('evalresultletternum')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rfq', function($table) {
            $table->dropColumn(['sampul', 'justifikasimetode', 'periode', 
                'sistemevaluasipenawaran', 'ownerestimate', 'invitetobidletternum', 
                'bidopeningletternum', 'memoadminevalletternum', 'technicalevalletternum', 
                'evalresultletternum'
            ]);
        });
    }
}
