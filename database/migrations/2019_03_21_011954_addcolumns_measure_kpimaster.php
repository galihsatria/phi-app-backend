<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddcolumnsMeasureKpimaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kpimaster', function($table) {
            $table->string('measure', 16)->nullable();
            $table->boolean('inverted')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kpimaster', function($table) {
            $table->dropColumn(['measure', 'inverted']);
        });
    }
}
