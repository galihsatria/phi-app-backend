<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidcontrolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bidcontrol', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rfqnum', 16)->index();
            $table->string('title');
            $table->string('department');
            // $table->unsignedInteger('buyer')->index();
            $table->string('buyer', 100);
            $table->date('openingdate')->nullable();
            $table->datetime('closingdate');
            $table->date('submitdate')->nullable();
            $table->datetime('bidopeningdate');
            $table->date('meetingdate')->nullable();
            $table->integer('vendorinvited')->default(0);
            $table->integer('vendorrejected')->default(0);
            $table->integer('vendorsubmitted')->default(0);
            $table->integer('vendorsubmittedontime')->nullable();
            $table->integer('vendorsubmittedlate')->nullable();
            $table->integer('vendornotsubmitted')->nullable();
            $table->string('routestatus', 16);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bidcontrol');
    }
}
