<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhidocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phidocuments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('owner', 16);
            $table->unsignedInteger('ownerid')->index();
            $table->string('category')->nullable();
            $table->string('docname', 400);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phidocuments');
    }
}
