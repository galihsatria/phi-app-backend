<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddcolumnClearpasswdPhivendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phivendor', function($table) {
            $table->string('clearpasswd', 16)->nullable();
            $table->string('useremail', 64)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phivendor', function($table) {
            $table->dropColumn(['clearpasswd', 'useremail']);
        });
    }
}
