<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorletterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_letter', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendorid', 16)->index();
            $table->string('lettertype', 8)->index();
            $table->string('description', 200);
            $table->date('sktvaliditydate')->nullable();
            $table->string('aktapendirianno')->nullable();
            $table->date('aktapendiriandate')->nullable();
            $table->string('aktaperubahanno')->nullable();
            $table->date('aktaperubahandate')->nullable();
            $table->string('buktipajak', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_letter');
    }
}
