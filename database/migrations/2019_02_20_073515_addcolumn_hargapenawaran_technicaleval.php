<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddcolumnHargapenawaranTechnicaleval extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('technicaleval', function($table) {
            $table->double('quoteprice')->nullable();
            $table->double('afternegoprice')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('technicaleval', function($table) {
            $table->dropColumn(['quoteprice', 'afternegoprice']);
        });
    }
}
