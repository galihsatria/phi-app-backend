<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpimasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpimaster', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 8);
            $table->string('description', 64);
            $table->double('targetmin', 8, 2)->nullable();
            $table->double('targetmed', 8, 2)->nullable();
            $table->double('targetmax', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpimaster');
    }
}
