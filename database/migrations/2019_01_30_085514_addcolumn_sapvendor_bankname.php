<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddcolumnSapvendorBankname extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor', function($table) {
            $table->string('bankname')->nullable();
            $table->string('bankaccountno')->nullable();
            $table->boolean('localupdated')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor', function($table) {
            $table->dropColumns(['bankname', 'bankaccountno', 'localupdated']);
        });
    }
}
