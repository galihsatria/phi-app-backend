<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenamecolumnRfqidWfapproval extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wfapproval', function (Blueprint $table) {
            $table->renameColumn('rfqid', 'ownerid');
            $table->string('ownertable');
            $table->string('payload', 2000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wfapproval', function (Blueprint $table) {
            $table->renameColumn('ownerid', 'rfqid');
            $table->dropColumn(['ownertable', 'payload']);
        });
    }
}
