<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddcolumnInvitationdateRfq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rfq', function($table) {
            $table->date('invitationdate')->nullable();
            $table->date('ambildokumendate')->nullable();

            $table->double('tkdn')->nullable();
            $table->string('fungsipengguna')->nullable();
            $table->double('estminvalue')->nullable();
            $table->double('estmaxvalue')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rfq', function($table) {
            $table->dropColumn([
                'invitationdate', 'ambildokumendate',
                'tkdn', 'fungsipengguna', 'estminvalue', 'estmaxvalue'
            ]);
        });
    }
}
