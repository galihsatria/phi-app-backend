<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bookingtype', 16); // bisa mobil, voucher, ruangan, dll
            $table->unsignedInteger('requestorid')->index();
            $table->string('origin')->nullable();
            $table->string('destination')->nullable();
            $table->datetime('departuredate')->nullable();
            $table->datetime('returndate')->nullable();
            $table->integer('quantity'); // kalau mobil 0, kalau voucher jadi jumlah voucher
            $table->string('status', 16); // requested, assigned, approved, completed
            $table->string('remarks')->nullable(); // kali-kali aja travel adminnya butuh corat coret di sini hehehe...
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}
