<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/testqr', function() {
    $pngImage = QrCode::format('png')->merge('logo.png', 0.15, true)
                        ->size(300)->errorCorrection('H')
                        ->generate('http://bit.ly/iwuei9809');
 
    return response($pngImage)->header('Content-type','image/png');
});

Route::get('/verifyqrcode/{whid}', 'HomeController@verifyQrCode');
Route::get('/getqrcode/{whid}', 'HomeController@getQrCode');
Route::get('/user/verify/{token}', 'HomeController@verifyUser');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/civd', 'HomeController@civd');
Route::get('/testsendpush', 'HomeController@sendpush');

Route::get('/sign', 'HomeController@signage');

Auth::routes(['verify' => true]);



// URL::forceScheme('https');
