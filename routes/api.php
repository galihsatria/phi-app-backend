<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function() {
    Route::get('/vendorlist', 'ApiController@vendorList');
    Route::post('/filtervendor', 'ApiController@filterVendor');
    Route::get('/vendordetail/{vendorid}', 'ApiController@vendorDetail');
    Route::post('/uploadvendorfile', 'ApiController@uploadVendorFile');
    Route::post('/uploadrfqfile', 'ApiController@uploadRfqFile');
    Route::post('/updatevendor', 'ApiController@updateVendor');
    Route::post('/savenewvendor', 'ApiController@saveNewVendor');
    Route::get('/getunavailableletter/{vendorid}', 'ApiController@unavailableLetter');
    Route::get('/getattachmentlist', 'ApiController@attachmentList');
    Route::get('/getcommoditylist', 'ApiController@commodityList');
    Route::get('/getcommoditylistfromavailablevendor', 'ApiController@commodityListVendor');
    Route::post('/savenewletter', 'ApiController@saveNewLetter');
    Route::post('/updateletter', 'ApiController@updateLetter');
    Route::post('/savecommodity', 'ApiController@saveCommodity');
    Route::post('/deletecommodity', 'ApiController@deleteCommodity');
    Route::post('/deleteattachment', 'ApiController@deleteAttachment');
    Route::get('/getvendorverification/{vendorid}', 'ApiController@vendorVerification');
    Route::post('/updateverificationlist', 'ApiController@updateVerificationList');


    Route::post('/deleterfqattachment', 'ApiController@deleteRfqAttachment');

    Route::post('/savebidcontrol', 'ApiController@saveBidControl');
    Route::get('/getbidcontrol', 'ApiController@getBidcontrol');
    Route::get('/getbuyer', 'ApiController@getBuyer');

    Route::post('/savenewprocurement', 'ApiController@saveNewProcurement');
    Route::post('/updateprocurement', 'ApiController@updateProcurement');
    Route::get('/deleteproc/{bidderid}', 'ApiController@deleteProcurement');
    Route::get('/getprocurementlist', 'ApiController@getProcurementList');
    Route::get('/getprocurement/{rfqid}', 'ApiController@getProcurement');
    Route::get('/getvendorbyid/{vendorid}', 'ApiController@vendorById');
    Route::post('/searchvendorbyname', 'ApiController@vendorByName');
    Route::post('/loadvendorbycommodity', 'ApiController@loadVendorByCommodity');
    Route::post('/getadminevallist', 'ApiController@adminEvalList');
    Route::post('/updateadmineval', 'ApiController@updateAdminEval');
    Route::post('/gettechnicaleval', 'ApiController@technicalEval');
    Route::post('/updatetechnicaleval', 'ApiController@updateTechnicalEval');
    Route::post('/getcommercialeval', 'ApiController@commercialEval');
    Route::post('/updatecommercialeval', 'ApiController@updateCommercialEval');
    Route::post('/updateambildokumen', 'ApiController@updateAmbilDokumen');
    Route::post('/updatekelengkapandokumen', 'ApiController@updateKelengkapanDokumen');
    Route::post('/updatebaep', 'ApiController@updateBaep');
    Route::get('/getbaep/{rfqid}', 'ApiController@baepList');
    Route::get('/getfungsipengguna', 'ApiController@getFungsiPenggunaList');
    Route::get('/getrfqmilestone/{rfqid}', 'ApiController@getRfqMilestone');

    Route::post('/startworkflow', 'BpmEngineController@startWorkflow');
    Route::post('/completeassignment', 'BpmEngineController@completeAssignment');
    Route::get('/getworkflowstatus/{rfqid}', 'BpmEngineController@workflowStatus');
    Route::get('/getworkflowhistory/{wfid}', 'BpmEngineController@workflowHistory');
    Route::get('/getinboxassignment/{userid}', 'BpmEngineController@inboxAssignment');
    Route::get('/getavailableworkflow/{rfqid}', 'BpmEngineController@availableWorkflow');
    
    Route::get('/getreports/{rfqid}', 'ApiController@getReports');
    Route::get('/dashboard', 'ApiController@dashboard');

    Route::post('/changepassword', 'ApiController@changePassword');

    Route::get('/kpilist/{period}', 'ApiController@kpilist');
    Route::post('/kpiupdate', 'ApiController@kpiupdate');
    Route::post('/kpiheaderupdate', 'ApiController@kpiheaderupdate');
    Route::post('/uploadkpifile', 'ApiController@kpiuploadfile');
    Route::post('/kpisubmit', 'ApiController@kpisubmit');
    Route::get('/kpidashboard', 'ApiController@kpidashboard');
    Route::get('/kpideletefile/{docid}', 'ApiController@kpiDeleteFile');
    Route::get('/kpidetail', 'ApiController@kpidetail');
    Route::get('/kpiattachmentlist/{kpiheaderid}', 'ApiController@kpiattachmentlist');

    Route::get('/myprofile', 'ApiController@getMyProfile');
    Route::post('/updatepushid', 'ApiController@updatePushId');

    // ---------- GENERAL AFFAIR OPERATIONS --------------------------
    Route::post('/savemobil', 'GeneralAffairController@saveMobil');
    Route::get('/getmobil', 'GeneralAffairController@getMobil');
    Route::post('/completebook', 'GeneralAffairController@completeBook');
    Route::get('/deletemobil/{vehicleid}', 'GeneralAffairController@deleteMobil');
    Route::get('/getbookingrequest', 'GeneralAffairController@getBookingRequest');
    Route::post('/savedriver', 'GeneralAffairController@saveDriver');
    Route::get('/deletedriver/{driverid}', 'GeneralAffairController@deleteDriver');
    Route::get('/destinations', 'GeneralAffairController@destinations');
    Route::post('/updateavatar', 'GeneralAffairController@updateAvatar');
    
    Route::get('/getmeetingroom', 'GeneralAffairController@meetingroom');
    Route::post('/saveroom', 'GeneralAffairController@saveRoom');
    Route::get('/deleteroom/{roomid}', 'GeneralAffairController@deleteRoom');
    Route::post('/saveschedule', 'GeneralAffairController@saveSchedule');
    Route::get('/deleteschedule/{scheduleid}', 'GeneralAffairController@deleteSchedule');
});

Route::middleware('auth:api')->group(function() {
    Auth::routes(['verify' => true]);
    Route::post('/savenewbook', 'VerifiedController@saveNewBook');
    Route::get('/myrequest', 'VerifiedController@myrequest');
    Route::get('/myapproval', 'VerifiedController@myapproval');
    Route::get('/approvaldetail/{wfid}', 'VerifiedController@approvalDetail');
    Route::get('/cancelrequest/{requestid}', 'VerifiedController@cancelRequest');
});

Route::post('/login', 'ApiController@login');
Route::post('/register', 'ApiController@register');
Route::get('/downloadattachment/{docid}/{handle?}', 'HomeController@downloadAttachment');
Route::get('/downloadrfqattachment/{docid}', 'HomeController@downloadRfqAttachment');

Route::get('/getdepartments', 'ApiController@getDepartments');
Route::get('/sendemail/{taskid}', 'BpmEngineController@sendEmail');
Route::get('/changestatus/{status}/{completedby}/{processinstanceid}', 'BpmEngineController@changeStatus');
